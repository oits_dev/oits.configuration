﻿namespace Oits.Configuration { 

	public interface IColumnName { 

		System.String ColumnName { 
			get;
			set;
		}

	}

}