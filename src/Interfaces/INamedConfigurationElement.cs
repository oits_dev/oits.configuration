﻿namespace Oits.Configuration { 

	/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.INamedConfigurationElement"]/member[@name=""]/*'/>
	public interface INamedConfigurationElement { 

		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.INamedConfigurationElement"]/member[@name="Name"]/*'/>
		System.String Name { 
			get;
			set;
		}

	}

}