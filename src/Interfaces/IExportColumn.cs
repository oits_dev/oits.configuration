﻿namespace Oits.Configuration { 

	public interface IExportColumn : INamedConfigurationElement { 

		System.String PropertyName { 
			get;
			set;
		}

		System.String HeaderText { 
			get;
			set;
		}

		System.String FormatString { 
			get;
			set;
		}

		System.String NullReplacementText { 
			get;
			set;
		}

		System.Nullable<System.Int32> Length { 
			get;
			set;
		}

		System.Boolean Skip { 
			get;
			set;
		}

	}

}