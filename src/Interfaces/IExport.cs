﻿namespace Oits.Configuration { 

	public interface IExport : INamedConfigurationElement { 

		System.String CultureName { 
			get;
			set;
		}

		System.Boolean UseDefaultCulture { 
			get;
			set;
		}

		System.Boolean SkipHeader { 
			get;
			set;
		}

		System.Nullable<System.Char> PaddingChar { 
			get;
			set;
		}

		System.String RecordSeparater { 
			get;
			set;
		}

		System.Nullable<System.Char> FieldSeparater { 
			get;
			set;
		}

		System.Char QuoteChar { 
			get;
			set;
		}

		System.Char EscapeChar { 
			get;
			set;
		}

		System.String Encoding { 
			get;
			set;
		}


		System.IO.Stream WriteExport( System.Collections.IEnumerable collection );
		void WriteExport( System.IO.Stream stream, System.Text.Encoding encoding, System.Collections.IEnumerable collection );

	}

}