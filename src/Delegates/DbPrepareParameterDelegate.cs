﻿using System.Linq;

namespace Oits.Configuration {

	/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration"]/member[@name="DbPrepareParameterDelegate(System.Object,Oits.Configuration.DbMap.DbPrepareParameterEventArgs)"]/*'/>
	public delegate void DbPrepareParameterDelegate( System.Object sender, DbMap.DbPrepareParameterEventArgs e );

}