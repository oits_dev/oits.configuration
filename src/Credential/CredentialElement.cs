﻿namespace Oits.Configuration.Credential { 

	/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Credential.CredentialElement"]/member[@name=""]/*'/>
	[System.Serializable]
	public class CredentialElement : System.Configuration.ConfigurationElement, INamedConfigurationElement { 


		#region .ctor
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Credential.CredentialElement"]/member[@name=".#ctor"]/*'/>
		public CredentialElement() : this( System.String.Empty, System.String.Empty, System.String.Empty, System.String.Empty ) { 
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Credential.CredentialElement"]/member[@name=".#ctor(System.String)"]/*'/>
		public CredentialElement( System.String name ) : this( name, System.String.Empty, System.String.Empty, System.String.Empty ) { 
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Credential.CredentialElement"]/member[@name=".#ctor(System.String,System.String,System.String,System.String)"]/*'/>
		public CredentialElement( System.String name, System.String userName, System.String password, System.String domain ) : base() { 
			this.Name = name;
			this.UserName = userName ?? System.String.Empty;
			this.Password = password ?? System.String.Empty;
			this.Domain = domain ?? System.String.Empty;
		}
		#endregion .ctor


		#region properties
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Credential.CredentialElement"]/member[@name="Name"]/*'/>
		[System.Configuration.ConfigurationProperty( "name", DefaultValue = "", IsRequired = true, IsKey = true )]
		public System.String Name { 
			get { 
				return (System.String)base[ "name" ];
			}
			set { 
				base[ "name" ] = value;
			}
		}

		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Credential.CredentialElement"]/member[@name="UserName"]/*'/>
		[System.Configuration.ConfigurationProperty( "userName", DefaultValue = "", IsRequired = true, IsKey = false )]
		public System.String UserName { 
			get { 
				return (System.String)base[ "userName" ];
			}
			set { 
				base[ "userName" ] = value ?? System.String.Empty;
			}
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Credential.CredentialElement"]/member[@name="Domain"]/*'/>
		[System.Configuration.ConfigurationProperty( "domain", DefaultValue = "", IsRequired = false, IsKey = false )]
		public System.String Domain { 
			get { 
				return (System.String)base[ "domain" ];
			}
			set { 
				base[ "domain" ] = value ?? System.String.Empty;
			}
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Credential.CredentialElement"]/member[@name="Password"]/*'/>
		[System.Configuration.ConfigurationProperty( "password", DefaultValue = "", IsRequired = false, IsKey = false )]
		public System.String Password { 
			get { 
				return (System.String)base[ "password" ];
			}
			set { 
				base[ "password" ] = value ?? System.String.Empty;
			}
		}
		#endregion properties


		#region methods
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Credential.CredentialElement"]/member[@name="ToNetworkCredential"]/*'/>
		public System.Net.NetworkCredential ToNetworkCredential() { 
			return new System.Net.NetworkCredential( this.UserName, this.Password, this.Domain );
		}
		#endregion methods


		#region static methods
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Credential.CredentialElement"]/member[@name="op_Explicit(Oits.Configuration.Credential.CredentialElement)~System.Net.NetworkCredential"]/*'/>
		public static explicit operator System.Net.NetworkCredential( CredentialElement element ) { 
			if ( null == element ) { 
				throw new System.ArgumentNullException( "element" );
			}
			return element.ToNetworkCredential();
		}
		#endregion static methods

	}

}