﻿namespace Oits.Configuration.Credential { 

	/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Credential.CredentialSection"]/member[@name=""]/*'/>
	[System.Serializable]
	public class CredentialSection : System.Configuration.ConfigurationSection { 

		#region fields
		/// <summary>The default name of the <see cref="Oits.Configuration.Credential.CredentialSection" /> element in the configuration file.</summary>
		public const System.String DefaultSectionName = "oits.credential";
		#endregion fields


		#region .ctor
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Credential.CredentialSection"]/member[@name=".#ctor"]/*'/>
		public CredentialSection() : base() { 
		}
		#endregion .ctor


		#region properties
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Credential.CredentialSection"]/member[@name="Credentials"]/*'/>
		[System.Configuration.ConfigurationProperty( "", IsDefaultCollection = true, IsRequired = false )]
		[System.Configuration.ConfigurationCollection( typeof( CredentialElementCollection ), 
			AddItemName = "add", 
			ClearItemsName = "clear", 
			RemoveItemName = "remove" 
		)]
		public CredentialElementCollection Credentials { 
			get { 
				return (CredentialElementCollection)base[ "" ];
			}
		}
		#endregion properties


		#region static methods
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Credential.CredentialSection"]/member[@name="GetSection"]/*'/>
		public static CredentialSection GetSection() { 
			return ( System.Configuration.ConfigurationManager.GetSection( Oits.Configuration.Credential.CredentialSection.DefaultSectionName ) as CredentialSection );
		}
		#endregion static methods

	}

}