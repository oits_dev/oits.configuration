﻿namespace Oits.Configuration.Credential { 

	/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Credential.CredentialElementCollection"]/member[@name=""]/*'/>
	[System.Serializable]
	public class CredentialElementCollection : Oits.Configuration.NamedConfigurationElementCollectionBase<CredentialElement> { 

		#region .ctor
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Credential.CredentialElementCollection"]/member[@name=".#ctor"]/*'/>
		public CredentialElementCollection() : base() { 
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Credential.CredentialElementCollection"]/member[@name=".#ctor(System.Collections.IComparer)"]/*'/>
		public CredentialElementCollection( System.Collections.IComparer comparer ) : base( comparer ) { 
		}
		#endregion .ctor

	}

}