﻿using System.Linq;

namespace Oits.Configuration.DbMap {

	/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbCommandElement"]/member[@name=""]/*'/>
	[System.Serializable]
	public class DbCommandElement : System.Configuration.ConfigurationElement, INamedConfigurationElement {

		#region .ctor
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbCommandElement"]/member[@name=".#ctor"]/*'/>
		public DbCommandElement() : base() {
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbCommandElement"]/member[@name=".#ctor(System.String)"]/*'/>
		public DbCommandElement( System.String name ) : this() {
			this.Name = name;
		}
		#endregion .ctor


		#region properties
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbCommandElement"]/member[@name="Name"]/*'/>
		[System.Configuration.ConfigurationProperty( "name", DefaultValue = "", IsRequired = true, IsKey = true )]
		public System.String Name {
			get {
				return (System.String)base[ "name" ];
			}
			set {
				base[ "name" ] = value;
			}
		}

		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbCommandElement"]/member[@name="CommandType"]/*'/>
		[System.Configuration.ConfigurationProperty( "commandType", DefaultValue = System.Data.CommandType.StoredProcedure, IsRequired = true )]
		public System.Data.CommandType CommandType {
			get {
				return (System.Data.CommandType)base[ "commandType" ];
			}
			set {
				base[ "commandType" ] = value;
			}
		}

		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbCommandElement"]/member[@name="CommandText"]/*'/>
		[System.Configuration.ConfigurationProperty( "commandText", IsRequired = true )]
		public System.String CommandText {
			get {
				return (System.String)base[ "commandText" ];
			}
			set {
				base[ "commandText" ] = value;
			}
		}

		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbCommandElement"]/member[@name="CommandTimeout"]/*'/>
		[System.Configuration.ConfigurationProperty( "commandTimeout", DefaultValue = 0, IsRequired = false )]
		public System.Int32 CommandTimeout {
			get {
				return (System.Int32)base[ "commandTimeout" ];
			}
			set {
				base[ "commandTimeout" ] = value;
			}
		}

		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbCommandElement"]/member[@name="UpdateRowSource"]/*'/>
		[System.Configuration.ConfigurationProperty( "updateRowSource", DefaultValue = System.Data.UpdateRowSource.None, IsRequired = false )]
		public System.Data.UpdateRowSource UpdateRowSource {
			get {
				return (System.Data.UpdateRowSource)base[ "updateRowSource" ];
			}
			set {
				base[ "updateRowSource" ] = value;
			}
		}

		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbCommandElement"]/member[@name="Parameters"]/*'/>
		[System.Configuration.ConfigurationProperty( "", IsDefaultCollection = true, IsRequired = false )]
		[System.Configuration.ConfigurationCollection( typeof( DbParameterCollection ),
			AddItemName = "add",
			ClearItemsName = "clear",
			RemoveItemName = "remove"
		)]
		public DbParameterCollection Parameters {
			get {
				return (DbParameterCollection)base[ "" ];
			}
		}
		#endregion properties


		#region methods
		private System.Data.Common.DbCommand ToCommandBase( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction ) {
			if ( null == connection ) {
				throw new System.ArgumentNullException( "connection" );
			}

			var output = connection.CreateCommand();
			output.Transaction = transaction;
			output.CommandText = this.CommandText;
			output.CommandType = this.CommandType;
			output.UpdatedRowSource = this.UpdateRowSource;
			if ( 0 != this.CommandTimeout ) {
				output.CommandTimeout = this.CommandTimeout;
			} else {
				output.CommandTimeout = connection.ConnectionTimeout;
			}

			return output;
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbCommandElement"]/member[@name="ToCommand(System.Data.Common.DbConnection,System.Data.Common.DbTransaction)"]/*'/>
		public System.Data.Common.DbCommand ToCommand( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction ) {
			if ( null == connection ) {
				throw new System.ArgumentNullException( "connection" );
			}

			var output = this.ToCommandBase( connection, transaction );
			foreach ( var p in this.Parameters.ToDbParameters( output ) ) {
				output.Parameters.Add( p );
			}
			return output;
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbCommandElement"]/member[@name="ToCommand(System.Data.Common.DbConnection,System.Data.Common.DbTransaction,System.Action`2)"]/*'/>
		public System.Data.Common.DbCommand ToCommand( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Action<System.Data.Common.DbCommand, DbCommandElement> prepare ) {
			if ( null == connection ) {
				throw new System.ArgumentNullException( "connection" );
			}

			var output = this.ToCommand( connection, transaction );
			if ( null != prepare ) {
				prepare( output, this );
			}
			return output;
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbCommandElement"]/member[@name="ToCommand(System.Data.Common.DbConnection,System.Data.Common.DbTransaction,System.Object)"]/*'/>
		public System.Data.Common.DbCommand ToCommand( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Object @object ) {
			if ( null == @object ) {
				throw new System.ArgumentNullException( "@object" );
			} else if ( null == connection ) {
				throw new System.ArgumentNullException( "connection" );
			}

			var output = this.ToCommand( connection, transaction );
			this.SetInputProperties( output, @object );
			return output;
		}


		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbCommandElement"]/member[@name="ToDbParameters(System.Data.Common.DbCommand)"]/*'/>
		public System.Collections.Generic.IEnumerable<System.Data.Common.DbParameter> ToDbParameters( System.Data.Common.DbCommand command ) {
			if ( null == command ) {
				throw new System.ArgumentNullException( "command" );
			}
			return this.Parameters.ToDbParameters( command );
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbCommandElement"]/member[@name="ToDbParameters(System.Data.Common.DbCommand,System.Action`1)"]/*'/>
		public System.Collections.Generic.IEnumerable<System.Data.Common.DbParameter> ToDbParameters( System.Data.Common.DbCommand command, System.Action<System.Data.Common.DbParameter> prepare ) {
			if ( null == command ) {
				throw new System.ArgumentNullException( "command" );
			}
			return this.Parameters.ToDbParameters( command, prepare: prepare );
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbCommandElement"]/member[@name="ToDbParameters(System.Data.Common.DbCommand,System.Object)"]/*'/>
		public System.Collections.Generic.IEnumerable<System.Data.Common.DbParameter> ToDbParameters( System.Data.Common.DbCommand command, System.Object @object ) {
			if ( null == @object ) {
				throw new System.ArgumentNullException( "object" );
			} else if ( null == command ) {
				throw new System.ArgumentNullException( "command" );
			}

			return this.Parameters.ToDbParameters( command, @object: @object );
		}

		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbCommandElement"]/member[@name="GetInputProperties(System.Type)"]/*'/>
		public System.Collections.Generic.IDictionary<DbParameterElement, System.Reflection.PropertyInfo> GetInputProperties( System.Type type ) {
			if ( null == type ) {
				throw new System.ArgumentNullException( "type" );
			}
			return this.Parameters.GetInputProperties( type );
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbCommandElement"]/member[@name="GetOutputProperties(System.Type)"]/*'/>
		public System.Collections.Generic.IDictionary<DbParameterElement, System.Reflection.PropertyInfo> GetOutputProperties( System.Type type ) {
			if ( null == type ) {
				throw new System.ArgumentNullException( "type" );
			}
			return this.Parameters.GetOutputProperties( type );
		}


		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbCommandElement"]/member[@name="SetInputProperties(System.Data.Common.DbCommand,System.Object)"]/*'/>
		public void SetInputProperties( System.Data.Common.DbCommand command, System.Object @object ) {
			if ( null == @object ) {
				throw new System.ArgumentNullException( "object" );
			} else if ( null == command ) {
				throw new System.ArgumentNullException( "command" );
			}

			var type = @object.GetType();
			var props = this.Parameters.GetInputProperties( type );
			foreach ( var kvp in props ) {
				command.Parameters[ kvp.Key.ParameterName ].Value = kvp.Value.GetValue( @object, null ) ?? System.DBNull.Value;
			}
		}
		public void SetInputProperties( System.Data.Common.DbCommand command, System.Object @object, System.Type type ) {
			if ( null == type ) {
				throw new System.ArgumentNullException( "type" );
			} else if ( null == @object ) {
				throw new System.ArgumentNullException( "object" );
			} else if ( null == command ) {
				throw new System.ArgumentNullException( "command" );
			}

			var props = this.Parameters.GetInputProperties( type );
			foreach ( var kvp in props ) {
				command.Parameters[ kvp.Key.ParameterName ].Value = kvp.Value.GetValue( @object, null ) ?? System.DBNull.Value;
			}
		}
		public void SetInputProperties( System.Data.Common.DbCommand command, System.Object @object, System.Collections.Generic.IDictionary<DbParameterElement, System.Reflection.PropertyInfo> props ) {
			if ( null == props ) {
				throw new System.ArgumentNullException( "props" );
			} else if ( null == @object ) {
				throw new System.ArgumentNullException( "object" );
			} else if ( null == command ) {
				throw new System.ArgumentNullException( "command" );
			}

			foreach ( var kvp in props ) {
				command.Parameters[ kvp.Key.ParameterName ].Value = kvp.Value.GetValue( @object, null ) ?? System.DBNull.Value;
			}
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbCommandElement"]/member[@name="SetOutputProperties(System.Data.Common.DbCommand,System.Object)"]/*'/>
		public void SetOutputProperties( System.Data.Common.DbCommand command, System.Object @object ) {
			if ( null == @object ) {
				throw new System.ArgumentNullException( "object" );
			} else if ( null == command ) {
				throw new System.ArgumentNullException( "command" );
			}

			var type = @object.GetType();
			var props = this.Parameters.GetOutputProperties( type );
			System.Object value;
			foreach ( var kvp in props ) {
				value = command.Parameters[ kvp.Key.ParameterName ].Value;
				if ( System.DBNull.Value.Equals( value ) ) {
					value = null;
				}
				kvp.Value.SetValue( @object, value, null );
			}
		}
		public void SetOutputProperties( System.Data.Common.DbCommand command, System.Object @object, System.Type type ) {
			if ( null == type ) {
				throw new System.ArgumentNullException( "type" );
			} else if ( null == @object ) {
				throw new System.ArgumentNullException( "object" );
			} else if ( null == command ) {
				throw new System.ArgumentNullException( "command" );
			}

			var props = this.Parameters.GetOutputProperties( type );
			System.Object value;
			foreach ( var kvp in props ) {
				value = command.Parameters[ kvp.Key.ParameterName ].Value;
				if ( System.DBNull.Value.Equals( value ) ) {
					value = null;
				}
				kvp.Value.SetValue( @object, value, null );
			}
		}
		public void SetOutputProperties( System.Data.Common.DbCommand command, System.Object @object, System.Collections.Generic.IDictionary<DbParameterElement, System.Reflection.PropertyInfo> props ) {
			if ( null == props ) {
				throw new System.ArgumentNullException( "props" );
			} else if ( null == @object ) {
				throw new System.ArgumentNullException( "object" );
			} else if ( null == command ) {
				throw new System.ArgumentNullException( "command" );
			}

			System.Object value;
			foreach ( var kvp in props ) {
				value = command.Parameters[ kvp.Key.ParameterName ].Value;
				if ( System.DBNull.Value.Equals( value ) ) {
					value = null;
				}
				kvp.Value.SetValue( @object, value, null );
			}
		}


		public System.Int32 ExecuteNonQuery( System.Data.Common.DbConnection connection ) {
			if ( null == connection ) {
				throw new System.ArgumentNullException( "connection" );
			}

			if ( System.Data.ConnectionState.Open != connection.State ) {
				connection.Open();
			}

			System.Int32 output = 0;
			using ( var cmd = this.ToCommand( connection, null ) ) {
				output = cmd.ExecuteNonQuery();
			}
			return output;
		}
		public System.Int32 ExecuteNonQuery( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction ) {
			if ( null == connection ) {
				throw new System.ArgumentNullException( "connection" );
			}

			if ( System.Data.ConnectionState.Open != connection.State ) {
				connection.Open();
			}

			System.Int32 output = 0;
			using ( var cmd = this.ToCommand( connection, transaction ) ) {
				output = cmd.ExecuteNonQuery();
			}
			return output;
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbCommandElement"]/member[@name="ExecuteNonQuery(System.Data.Common.DbConnection,System.Object)"]/*'/>
		public System.Int32 ExecuteNonQuery( System.Data.Common.DbConnection connection, System.Object @object ) {
			if ( null == connection ) {
				throw new System.ArgumentNullException( "connection" );
			}

			if ( System.Data.ConnectionState.Open != connection.State ) {
				connection.Open();
			}

			System.Int32 output = 0;
			using ( var cmd = this.ToCommand( connection, null ) ) {
				if ( null != @object ) {
					this.SetInputProperties( cmd, @object );
				}
				output = cmd.ExecuteNonQuery();
				if ( null != @object ) {
					this.SetOutputProperties( cmd, @object );
				}
			}

			return output;
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbCommandElement"]/member[@name="ExecuteNonQuery(System.Data.Common.DbConnection,System.Data.Common.DbTransaction,System.Object)"]/*'/>
		public System.Int32 ExecuteNonQuery( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Object @object ) {
			if ( null == connection ) {
				throw new System.ArgumentNullException( "connection" );
			}

			if ( System.Data.ConnectionState.Open != connection.State ) {
				connection.Open();
			}

			System.Int32 output = 0;
			using ( var cmd = this.ToCommand( connection, transaction ) ) {
				if ( null != @object ) {
					this.SetInputProperties( cmd, @object );
				}
				output = cmd.ExecuteNonQuery();
				if ( null != @object ) {
					this.SetOutputProperties( cmd, @object );
				}
			}

			return output;
		}
		public System.Int32 ExecuteNonQuery<T>( System.Data.Common.DbConnection connection, System.Collections.Generic.IEnumerable<T> collection ) {
			if ( null == collection ) {
				throw new System.ArgumentNullException( "collection" );
			} else if ( null == connection ) {
				throw new System.ArgumentNullException( "connection" );
			}

			if ( System.Data.ConnectionState.Open != connection.State ) {
				connection.Open();
			}

			System.Int32 output = 0;
			using ( var cmd = this.ToCommand( connection, null ) ) {
				var inputProps = this.Parameters.GetInputProperties( typeof( T ) );
				var outputProps = this.Parameters.GetOutputProperties( typeof( T ) );
				foreach ( var item in collection ) {
					if ( null != item ) {
						this.SetInputProperties( cmd, item, inputProps );
					}
					cmd.Prepare();
					output += cmd.ExecuteNonQuery();
					if ( null != item ) {
						this.SetOutputProperties( cmd, item, outputProps );
					}
				}
			}

			return output;
		}
		public System.Int32 ExecuteNonQuery<T>( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Collections.Generic.IEnumerable<T> collection ) {
			if ( null == collection ) {
				throw new System.ArgumentNullException( "collection" );
			} else if ( null == connection ) {
				throw new System.ArgumentNullException( "connection" );
			}

			if ( System.Data.ConnectionState.Open != connection.State ) {
				connection.Open();
			}

			System.Int32 output = 0;
			using ( var cmd = this.ToCommand( connection, transaction ) ) {
				var inputProps = this.Parameters.GetInputProperties( typeof( T ) );
				var outputProps = this.Parameters.GetOutputProperties( typeof( T ) );
				foreach ( var item in collection ) {
					if ( null != item ) {
						this.SetInputProperties( cmd, item, inputProps );
					}
					cmd.Prepare();
					output += cmd.ExecuteNonQuery();
					if ( null != item ) {
						this.SetOutputProperties( cmd, item, outputProps );
					}
				}
			}

			return output;
		}


		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbCommandElement"]/member[@name="ExecuteNullableScalar`1(System.Data.Common.DbConnection,System.Data.Common.DbTransaction,System.Object,System.Func`2)"]/*'/>
		public System.Nullable<R> ExecuteNullableScalar<R>( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Object @object, System.Func<System.Object, R> cast ) where R : struct {
			if ( null == cast ) {
				throw new System.ArgumentNullException( "cast" );
			} else if ( null == connection ) {
				throw new System.ArgumentNullException( "connection" );
			}

			if ( System.Data.ConnectionState.Open != connection.State ) {
				connection.Open();
			}

			System.Nullable<R> output = null;

			using ( var cmd = this.ToCommand( connection, transaction ) ) {
				if ( null != @object ) {
					this.SetInputProperties( cmd, @object );
				}
				System.Object q = cmd.ExecuteScalar();
				if ( null != @object ) {
					this.SetOutputProperties( cmd, @object );
				}
				if ( ( null != q ) && ( !System.DBNull.Value.Equals( q ) ) ) {
					output = cast( q );
				}
			}

			return output;
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbCommandElement"]/member[@name="ExecuteScalar`1(System.Data.Common.DbConnection,System.Data.Common.DbTransaction,System.Object,System.Func`2)"]/*'/>
		public R ExecuteScalar<R>( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Object @object, System.Func<System.Object, R> cast ) {
			if ( null == cast ) {
				throw new System.ArgumentNullException( "cast" );
			} else if ( null == connection ) {
				throw new System.ArgumentNullException( "connection" );
			}

			if ( System.Data.ConnectionState.Open != connection.State ) {
				connection.Open();
			}

			R output = default( R );

			using ( var cmd = this.ToCommand( connection, transaction ) ) {
				if ( null != @object ) {
					this.SetInputProperties( cmd, @object );
				}
				System.Object q = cmd.ExecuteScalar();
				if ( null != @object ) {
					this.SetOutputProperties( cmd, @object );
				}
				output = cast( q );
			}

			return output;
		}

		public System.Data.IDataReader ExecuteReaderSingleRow( System.Data.Common.DbCommand command, System.Data.CommandBehavior commandBehavior ) {
			if ( null == command ) {
				throw new System.ArgumentNullException( "command" );
			} else if ( null == command.Connection ) {
				throw new System.ArgumentException();
			}

			if ( System.Data.ConnectionState.Open != command.Connection.State ) {
				command.Connection.Open();
			}

			return command.ExecuteReader( commandBehavior | System.Data.CommandBehavior.SingleResult | System.Data.CommandBehavior.SingleRow );
		}
		public System.Data.IDataReader ExecuteReaderSingleResult( System.Data.Common.DbCommand command, System.Data.CommandBehavior commandBehavior ) {
			if ( null == command ) {
				throw new System.ArgumentNullException( "command" );
			} else if ( null == command.Connection ) {
				throw new System.ArgumentException();
			}

			if ( System.Data.ConnectionState.Open != command.Connection.State ) {
				command.Connection.Open();
			}

			return command.ExecuteReader( commandBehavior | System.Data.CommandBehavior.SingleResult );
		}
		public System.Data.IDataReader ExecuteReader( System.Data.Common.DbCommand command, System.Data.CommandBehavior commandBehavior ) {
			if ( null == command ) {
				throw new System.ArgumentNullException( "command" );
			} else if ( null == command.Connection ) {
				throw new System.ArgumentException();
			}

			if ( System.Data.ConnectionState.Open != command.Connection.State ) {
				command.Connection.Open();
			}

			return command.ExecuteReader( commandBehavior );
		}

		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbCommandElement"]/member[@name="ExecuteReaderSingleRow`1(System.Data.Common.DbConnection,System.Data.Common.DbTransaction,System.Object,Oits.Configuration.DbMap.DbResultElement,System.Func`3)"]/*'/>
		public R ExecuteReaderSingleRow<R>( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Object @object, Oits.Configuration.DbMap.DbResultElement result, System.Func<System.Data.Common.DbDataReader, Oits.Configuration.DbMap.DbResultElement, R> read ) {
			if ( null == result ) {
				throw new System.ArgumentNullException( "result" );
			} else if ( null == connection ) {
				throw new System.ArgumentNullException( "connection" );
			}

			if ( System.Data.ConnectionState.Open != connection.State ) {
				connection.Open();
			}

			R r = default( R );
			using ( var cmd = this.ToCommand( connection, transaction ) ) {
				if ( null != @object ) {
					this.SetInputProperties( cmd, @object );
				}
				using ( var reader = cmd.ExecuteReader( System.Data.CommandBehavior.SingleResult | System.Data.CommandBehavior.SingleRow ) ) {
					if ( reader.HasRows && reader.Read() ) {
						r = read( reader, result );
					}
					if ( null != @object ) {
						this.SetOutputProperties( cmd, @object );
					}
				}
			}

			return r;
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbCommandElement"]/member[@name="ExecuteReaderSingleRow`1(System.Data.Common.DbConnection,System.Data.Common.DbTransaction,System.Object,Oits.Configuration.DbMap.DbResultElement)"]/*'/>
		public R ExecuteReaderSingleRow<R>( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Object @object, DbResultElement result ) where R : new() {
			return this.ExecuteReaderSingleRow<R>( connection, transaction, @object, System.Data.CommandBehavior.SingleResult | System.Data.CommandBehavior.SingleRow, result );
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbCommandElement"]/member[@name="ExecuteReaderSingleRow`1(System.Data.Common.DbConnection,System.Data.Common.DbTransaction,System.Object,System.Data.CommandBehavior,Oits.Configuration.DbMap.DbResultElement)"]/*'/>
		public R ExecuteReaderSingleRow<R>( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Object @object, System.Data.CommandBehavior commandBehavior, DbResultElement result ) where R : new() {
			if ( null == result ) {
				throw new System.ArgumentNullException( "result" );
			} else if ( null == connection ) {
				throw new System.ArgumentNullException( "connection" );
			}

			if ( System.Data.ConnectionState.Open != connection.State ) {
				connection.Open();
			}

			R r = default( R );
			using ( var cmd = this.ToCommand( connection, transaction ) ) {
				if ( null != @object ) {
					this.SetInputProperties( cmd, @object );
				}
				using ( var rdr = cmd.ExecuteReader( commandBehavior | System.Data.CommandBehavior.SingleResult | System.Data.CommandBehavior.SingleRow ) ) {
					if ( rdr.Read() ) {
						r = result.GetResult<R>( rdr );
					}
					if ( null != @object ) {
						this.SetOutputProperties( cmd, @object );
					}
				}
			}
			return r;
		}


		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbCommandElement"]/member[@name="ExecuteReaderSingleResult`1(System.Data.Common.DbConnection,System.Data.Common.DbTransaction,System.Action`2,System.Action`2,System.Data.CommandBehavior,Oits.Configuration.DbMap.DbResultElement,System.Action`2,System.Func`3, System.Action`2)"]/*'/>
		public System.Collections.Generic.IList<R> ExecuteReaderSingleResult<R>( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Action<System.Data.Common.DbCommand, Oits.Configuration.DbMap.DbCommandElement> preExecute, System.Action<System.Data.Common.DbCommand, Oits.Configuration.DbMap.DbCommandElement> postExecute, System.Data.CommandBehavior commandBehavior, Oits.Configuration.DbMap.DbResultElement result, System.Action<System.Data.Common.DbDataReader, Oits.Configuration.DbMap.DbResultElement> preRead, System.Func<System.Data.Common.DbDataReader, Oits.Configuration.DbMap.DbResultElement, R> read, System.Action<System.Data.Common.DbDataReader, Oits.Configuration.DbMap.DbResultElement> postRead ) {
			if ( null == read ) {
				throw new System.ArgumentNullException( "read" );
			} else if ( null == result ) {
				throw new System.ArgumentNullException( "result" );
			} else if ( null == connection ) {
				throw new System.ArgumentNullException( "connection" );
			}

			if ( System.Data.ConnectionState.Open != connection.State ) {
				connection.Open();
			}

			var output = new System.Collections.Generic.List<R>();

			using ( var cmd = this.ToCommand( connection, transaction, preExecute ) ) {
				using ( var rdr = cmd.ExecuteReader( commandBehavior | System.Data.CommandBehavior.SingleResult ) ) {
					if ( null != preRead ) {
						preRead( rdr, result );
					}
					if ( rdr.HasRows ) {
						while ( !rdr.IsClosed && rdr.Read() ) {
							output.Add( read( rdr, result ) );
						}
					}
					if ( null != postRead ) {
						postRead( rdr, result );
					}
					if ( null != postExecute ) {
						postExecute( cmd, this );
					}
				}
			}

			return output;
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbCommandElement"]/member[@name="ExecuteReaderSingleResult`1(System.Data.Common.DbConnection,System.Data.Common.DbTransaction,System.Action`2,System.Action`2,Oits.Configuration.DbMap.DbResultElement,System.Action`2,System.Func`3)"]/*'/>
		public System.Collections.Generic.IList<R> ExecuteReaderSingleResult<R>( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Action<System.Data.Common.DbCommand, Oits.Configuration.DbMap.DbCommandElement> preExecute, System.Action<System.Data.Common.DbCommand, Oits.Configuration.DbMap.DbCommandElement> postExecute, Oits.Configuration.DbMap.DbResultElement result, System.Action<System.Data.Common.DbDataReader, Oits.Configuration.DbMap.DbResultElement> preRead, System.Func<System.Data.Common.DbDataReader, Oits.Configuration.DbMap.DbResultElement, R> read ) {
			return this.ExecuteReaderSingleResult<R>( connection, transaction, preExecute, postExecute, System.Data.CommandBehavior.SingleResult, result, preRead, read, null );
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbCommandElement"]/member[@name="ExecuteReaderSingleResult`1(System.Data.Common.DbConnection,System.Data.Common.DbTransaction,System.Object,Oits.Configuration.DbMap.DbResultElement)"]/*'/>
		public System.Collections.Generic.IList<R> ExecuteReaderSingleResult<R>( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Object @object, Oits.Configuration.DbMap.DbResultElement result ) where R : new() {
			return this.ExecuteReaderSingleResult<R>( connection, transaction, @object, System.Data.CommandBehavior.SingleResult, result );
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbCommandElement"]/member[@name="ExecuteReaderSingleResult`1(System.Data.Common.DbConnection,System.Data.Common.DbTransaction,System.Object,System.Data.CommandBehavior,Oits.Configuration.DbMap.DbResultElement)"]/*'/>
		public System.Collections.Generic.IList<R> ExecuteReaderSingleResult<R>( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Object @object, System.Data.CommandBehavior commandBehavior, Oits.Configuration.DbMap.DbResultElement result ) where R : new() {
			if ( null == result ) {
				throw new System.ArgumentNullException( "result" );
			} else if ( null == connection ) {
				throw new System.ArgumentNullException( "connection" );
			}

			if ( System.Data.ConnectionState.Open != connection.State ) {
				connection.Open();
			}

			var output = new System.Collections.Generic.List<R>();

			using ( var cmd = this.ToCommand( connection, transaction ) ) {
				if ( null != @object ) {
					this.SetInputProperties( cmd, @object );
				}
				using ( var rdr = cmd.ExecuteReader( commandBehavior | System.Data.CommandBehavior.SingleResult ) ) {
					if ( !rdr.IsClosed && rdr.HasRows ) {
						var ords = result.GetOrdinals( rdr );
						var props = result.GetProperties( typeof( R ) );
						while ( rdr.Read() ) {
							output.Add( result.GetResult<R>( rdr, ords, props ) );
						}
					}
					if ( null != @object ) {
						this.SetOutputProperties( cmd, @object );
					}
				}
			}

			return output;
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbCommandElement"]/member[@name="ExecuteReaderSingleResult`1(System.Data.Common.DbConnection,System.Data.Common.DbTransaction,System.Object,Oits.Configuration.DbMap.DbResultElement,System.Func`3)"]/*'/>
		public System.Collections.Generic.IList<R> ExecuteReaderSingleResult<R>( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Object @object, DbResultElement result, System.Func<System.Data.Common.DbDataReader, Oits.Configuration.DbMap.DbResultElement, R> read ) {
			if ( null == result ) {
				throw new System.ArgumentNullException( "result" );
			} else if ( null == connection ) {
				throw new System.ArgumentNullException( "connection" );
			}

			if ( System.Data.ConnectionState.Open != connection.State ) {
				connection.Open();
			}

			var output = new System.Collections.Generic.List<R>();

			using ( var cmd = this.ToCommand( connection, transaction ) ) {
				if ( null != @object ) {
					this.SetInputProperties( cmd, @object );
				}
				using ( var rdr = cmd.ExecuteReader( System.Data.CommandBehavior.SingleResult ) ) {
					if ( !rdr.IsClosed && rdr.HasRows ) {
						while ( rdr.Read() ) {
							output.Add( read( rdr, result ) );
						}
						if ( null != @object ) {
							this.SetOutputProperties( cmd, @object );
						}
					}
				}
			}

			return output;
		}


		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbCommandElement"]/member[@name="ExecuteReaderSingleResult`2(System.Data.Common.DbConnection,System.Data.Common.DbTransaction,System.Object,Oits.Configuration.DbMap.DbResultElement,System.Func`2)"]/*'/>
		public System.Collections.Generic.IDictionary<K, V> ExecuteReaderSingleResult<K, V>( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Object @object, DbResultElement result, System.Func<V, K> key ) where V : new() {
			return this.ExecuteReaderSingleResult<K, V>( connection, transaction, @object, System.Data.CommandBehavior.SingleResult, result, key );
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbCommandElement"]/member[@name="ExecuteReaderSingleResult`2(System.Data.Common.DbConnection,System.Data.Common.DbTransaction,System.Object,System.Data.CommandBehavior,Oits.Configuration.DbMap.DbResultElement,System.Func`2)"]/*'/>
		public System.Collections.Generic.IDictionary<K, V> ExecuteReaderSingleResult<K, V>( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Object @object, System.Data.CommandBehavior commandBehavior, DbResultElement result, System.Func<V, K> key ) where V : new() {
			if ( null == result ) {
				throw new System.ArgumentNullException( "result" );
			} else if ( null == connection ) {
				throw new System.ArgumentNullException( "connection" );
			}

			if ( System.Data.ConnectionState.Open != connection.State ) {
				connection.Open();
			}

			var output = new System.Collections.Generic.Dictionary<K, V>();

			using ( var cmd = this.ToCommand( connection, transaction ) ) {
				if ( null != @object ) {
					this.SetInputProperties( cmd, @object );
				}
				using ( var rdr = cmd.ExecuteReader( commandBehavior | System.Data.CommandBehavior.SingleResult ) ) {
					if ( !rdr.IsClosed && rdr.HasRows ) {
						V item;
						var ords = result.GetOrdinals( rdr );
						var props = result.GetProperties( typeof( V ) );
						while ( !rdr.IsClosed && rdr.Read() ) {
							item = result.GetResult<V>( rdr, ords, props );
							output.Add( key( item ), item );
						}
					}
					if ( null != @object ) {
						this.SetOutputProperties( cmd, @object );
					}
				}
			}

			return output;
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbCommandElement"]/member[@name="ExecuteReaderSingleResult`2(System.Data.Common.DbConnection,System.Data.Common.DbTransaction,System.Object,System.Data.CommandBehavior,Oits.Configuration.DbMap.DbResultElement,System.Func`3,System.Func`2)"]/*'/>
		public System.Collections.Generic.IDictionary<K, V> ExecuteReaderSingleResult<K, V>( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Object @object, DbResultElement result, System.Func<System.Data.Common.DbDataReader, Oits.Configuration.DbMap.DbResultElement, V> read, System.Func<V, K> key ) {
			if ( null == result ) {
				throw new System.ArgumentNullException( "result" );
			} else if ( null == connection ) {
				throw new System.ArgumentNullException( "connection" );
			}

			if ( System.Data.ConnectionState.Open != connection.State ) {
				connection.Open();
			}

			var output = new System.Collections.Generic.Dictionary<K, V>();

			using ( var cmd = this.ToCommand( connection, transaction ) ) {
				if ( null != @object ) {
					this.SetInputProperties( cmd, @object );
				}
				using ( var rdr = cmd.ExecuteReader( System.Data.CommandBehavior.SingleResult ) ) {
					V item;
					while ( !rdr.IsClosed && rdr.Read() ) {
						item = read( rdr, result );
						output.Add( key( item ), item );
					}
					if ( null != @object ) {
						this.SetOutputProperties( cmd, @object );
					}
				}
			}

			return output;
		}


		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbCommandElement"]/member[@name="ExecuteXmlReader(System.Data.Common.DbConnection,System.Data.Common.DbTransaction,System.Object)"]/*'/>
		public System.String ExecuteXmlReader( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Object @object ) {
			if ( null == connection ) {
				throw new System.ArgumentNullException( "connection" );
			}

			if ( System.Data.ConnectionState.Open != connection.State ) {
				connection.Open();
			}

			System.String output = null;

			using ( var cmd = this.ToCommand( connection, transaction ) ) {
				if ( null != @object ) {
					this.SetInputProperties( cmd, @object );
				}
				System.Data.SqlClient.SqlCommand sql = ( cmd as System.Data.SqlClient.SqlCommand );
				if ( null == sql ) {
					throw new System.InvalidOperationException();
				}
				using ( var reader = ExecuteXmlReader( sql ) ) {
					var sb = new System.Text.StringBuilder();
					using ( var writer = new System.IO.StringWriter( sb ) ) {
						while ( reader.Read() ) {
							writer.Write( reader.ReadOuterXml() );
						}
					}
					if ( 0 < sb.Length ) {
						output = sb.ToString();
					}
					if ( null != @object ) {
						this.SetOutputProperties( cmd, @object );
					}
				}
			}

			return output;
		}

		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbCommandElement"]/member[@name="ExecuteXmlReader`1(System.Data.Common.DbConnection,System.Data.Common.DbTransaction,System.Object,System.Func`2)"]/*'/>
		public R ExecuteXmlReader<R>( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Object @object, System.Func<System.Xml.XmlReader, R> cast ) {
			if ( null == cast ) {
				throw new System.ArgumentNullException( "cast" );
			} else if ( null == connection ) {
				throw new System.ArgumentNullException( "connection" );
			}

			if ( System.Data.ConnectionState.Open != connection.State ) {
				connection.Open();
			}

			R output = default( R );

			using ( var cmd = this.ToCommand( connection, transaction ) ) {
				if ( null != @object ) {
					this.SetInputProperties( cmd, @object );
				}
				System.Data.SqlClient.SqlCommand sql = ( cmd as System.Data.SqlClient.SqlCommand );
				if ( null == sql ) {
					throw new System.InvalidOperationException();
				}
				using ( var reader = sql.ExecuteXmlReader() ) {
					output = cast( reader );
					if ( null != @object ) {
						this.SetOutputProperties( cmd, @object );
					}
				}
			}

			return output;
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbCommandElement"]/member[@name="ExecuteXmlReader`1(System.Data.Common.DbConnection,System.Data.Common.DbTransaction,System.Object,System.Func`2,System.Xml.Xsl.XslCompiledTransform)"]/*'/>
		public R ExecuteXmlReader<R>( System.Data.Common.DbConnection connection, System.Data.Common.DbTransaction transaction, System.Object @object, System.Func<System.Object, R> cast, System.Xml.Xsl.XslCompiledTransform transform ) {
			if ( null == cast ) {
				throw new System.ArgumentNullException( "cast" );
			} else if ( null == connection ) {
				throw new System.ArgumentNullException( "connection" );
			}

			if ( System.Data.ConnectionState.Open != connection.State ) {
				connection.Open();
			}

			R output = default( R );

			using ( var stream = new System.IO.StringReader( this.ExecuteXmlReader( connection, transaction, @object ) ) ) {
				using ( var reader = System.Xml.XmlReader.Create( stream ) ) {
					if ( null == transform ) {
						output = ExecuteXmlReader<R>( reader, cast );
					} else {
						output = ExecuteXmlReader<R>( reader, transform, cast );
					}
				}
			}

			return output;
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbCommandElement"]/member[@name="ExecuteXmlReader`1(System.Xml.XmlReader,System.Func`2)"]/*'/>
		public R ExecuteXmlReader<R>( System.Xml.XmlReader source, System.Func<System.Object, R> cast ) {
			if ( null == cast ) {
				throw new System.ArgumentNullException( "cast" );
			} else if ( null == source ) {
				throw new System.ArgumentNullException( "source" );
			}

			R output = default( R );

			var loader = new System.Xml.Serialization.XmlSerializer( typeof( R ) );
			output = cast( loader.Deserialize( source ) );

			return output;
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbCommandElement"]/member[@name="ExecuteXmlReader`1(System.Xml.XmlReader,System.Xml.Xsl.XslCompiledTransform,System.Func`2)"]/*'/>
		public R ExecuteXmlReader<R>( System.Xml.XmlReader source, System.Xml.Xsl.XslCompiledTransform transform, System.Func<System.Object, R> cast ) {
			if ( null == cast ) {
				throw new System.ArgumentNullException( "cast" );
			} else if ( null == transform ) {
				throw new System.ArgumentNullException( "transform" );
			} else if ( null == source ) {
				throw new System.ArgumentNullException( "source" );
			}

			R output = default( R );

			var shimXml = new System.Text.StringBuilder();
			using ( var writer = System.Xml.XmlWriter.Create( shimXml ) ) {
				transform.Transform( source, writer );
			}
			using ( var stream = new System.IO.StringReader( shimXml.ToString() ) ) {
				using ( var reader = System.Xml.XmlReader.Create( stream ) ) {
					var loader = new System.Xml.Serialization.XmlSerializer( typeof( R ) );
					output = cast( loader.Deserialize( reader ) );
				}
			}

			return output;
		}
		#endregion methods


		#region static methods
		private static System.Xml.XmlReader ExecuteXmlReader( System.Data.SqlClient.SqlCommand command ) {
			if ( null == command ) {
				throw new System.ArgumentNullException( "command" );
			}

			return command.ExecuteXmlReader();
		}
		#endregion static methods

	}

}