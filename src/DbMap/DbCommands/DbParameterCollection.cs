﻿using System.Linq;

namespace Oits.Configuration.DbMap { 

	/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbParameterCollection"]/member[@name=""]/*'/>
	[System.Serializable]
	public class DbParameterCollection : NamedConfigurationElementCollectionBase<DbParameterElement> { 

		#region .ctor
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbParameterCollection"]/member[@name=".#ctor"]/*'/>
		public DbParameterCollection() : base() { 
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbParameterCollection"]/member[@name=".#ctor(System.Collections.IComparer)"]/*'/>
		public DbParameterCollection( System.Collections.IComparer comparer ) : base( comparer ) { 
		}
		#endregion .ctor


		#region methods
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbParameterCollection"]/member[@name="ToDbParameters(System.Data.Common.DbCommand)"]/*'/>
		public System.Collections.Generic.IEnumerable<System.Data.Common.DbParameter> ToDbParameters( System.Data.Common.DbCommand command ) { 
			if ( null == command ) { 
				throw new System.ArgumentNullException( "command" );
			}
			foreach ( var p in this.OfType<DbParameterElement>().Where( 
				x => null != x 
			) ) { 
				yield return p.ToDbParameter( command );
			}
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbParameterCollection"]/member[@name="ToDbParameters(System.Data.Common.DbCommand,System.Action`1)"]/*'/>
		public System.Collections.Generic.IEnumerable<System.Data.Common.DbParameter> ToDbParameters( System.Data.Common.DbCommand command, System.Action<System.Data.Common.DbParameter> prepare ) { 
			if ( null == command ) { 
				throw new System.ArgumentNullException( "command" );
			}
			foreach ( var p in this.OfType<DbParameterElement>().Where( 
				x => null != x 
			) ) { 
				yield return p.ToDbParameter( command, prepare: prepare );
			}
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbParameterCollection"]/member[@name="ToDbParameters(System.Data.Common.DbCommand,System.Object)"]/*'/>
		public System.Collections.Generic.IEnumerable<System.Data.Common.DbParameter> ToDbParameters( System.Data.Common.DbCommand command, System.Object @object ) { 
			if ( null == @object ) { 
				throw new System.ArgumentNullException( "object" );
			} else if ( null == command ) { 
				throw new System.ArgumentNullException( "command" );
			}

			var pi = this.GetInputProperties( @object.GetType() );
			foreach ( var p in this.OfType<DbParameterElement>().Where( 
				x => null != x 
			) ) { 
				if ( pi.ContainsKey( p ) ) { 
					yield return p.ToDbParameter( command, pi[ p ], @object );
				} else { 
					yield return p.ToDbParameter( command );
				}
			}
		}

		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbParameterCollection"]/member[@name="GetInputProperties(System.Type)"]/*'/>
		public System.Collections.Generic.IDictionary<DbParameterElement, System.Reflection.PropertyInfo> GetInputProperties( System.Type type ) { 
			if ( null == type ) { 
				throw new System.ArgumentNullException( "type" );
			}

			var map = new System.Collections.Generic.Dictionary<DbParameterElement, System.Reflection.PropertyInfo>( this.Count );
			System.Reflection.PropertyInfo pi;
			foreach ( var p in this.OfType<DbParameterElement>().Where( 
				x => ( null != x ) 
			).Where( 
				x => ( 
					( System.Data.ParameterDirection.Input == x.Direction ) 
					|| ( System.Data.ParameterDirection.InputOutput == x.Direction ) 
				) 
			) ) { 
				pi = p.GetInputProperty( type );
				if ( null != pi ) { 
					map.Add( p, pi );
				}
			}
			return map;
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbParameterCollection"]/member[@name="GetOutputProperties(System.Type)"]/*'/>
		public System.Collections.Generic.IDictionary<DbParameterElement, System.Reflection.PropertyInfo> GetOutputProperties( System.Type type ) { 
			if ( null == type ) { 
				throw new System.ArgumentNullException( "type" );
			}

			var map = new System.Collections.Generic.Dictionary<DbParameterElement, System.Reflection.PropertyInfo>( this.Count );
			System.Reflection.PropertyInfo pi;
			foreach ( var p in this.OfType<DbParameterElement>().Where( 
				x => ( null != x ) 
			).Where( 
				x => ( 
					( System.Data.ParameterDirection.Output == x.Direction ) 
					|| ( System.Data.ParameterDirection.InputOutput == x.Direction ) 
					|| ( System.Data.ParameterDirection.ReturnValue == x.Direction ) 
				) 
			) ) { 
				pi = p.GetOutputProperty( type );
				if ( null != pi ) { 
					map.Add( p, pi );
				}
			}
			return map;
		}
		#endregion methods

	}

}