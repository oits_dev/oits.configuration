﻿using System.Linq;

namespace Oits.Configuration.DbMap { 

	/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbCommandCollection"]/member[@name=""]/*'/>
	[System.Serializable]
	public class DbCommandCollection : NamedConfigurationElementCollectionBase<DbCommandElement> { 

		#region .ctor
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbCommandCollection"]/member[@name=".#ctor"]/*'/>
		public DbCommandCollection() : base() { 
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbCommandCollection"]/member[@name=".#ctor(System.Collections.IComparer)"]/*'/>
		public DbCommandCollection( System.Collections.IComparer comparer ) : base( comparer ) { 
		}
		#endregion .ctor

	}

}