﻿using System.Linq;

namespace Oits.Configuration.DbMap {

	public static class DataReaderHelper {

		private const System.Int32 BufferLength = 16384;

		// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DataReaderHelper"]/member[@name="GetOrdinal(System.Data.Common.DbDataReader)"]/*'/>
		public static System.Int32 GetOrdinal( this System.Data.IDataReader reader, IDbColumn column ) {
			if ( null == column ) {
				throw new System.ArgumentNullException( "column" );
			} else if ( ( null == reader ) || reader.IsClosed ) {
				throw new System.ArgumentNullException( "reader" );
			}
			var columnName = column.ColumnName;
			if ( System.String.IsNullOrEmpty( columnName ) ) {
				columnName = column.Name;
			}
			return reader.GetOrdinal( columnName );
		}
		// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DataReaderHelper"]/member[@name="TryGetOrdinal(System.Data.Common.DbDataReader,System.Int32)"]/*'/>
		public static System.Boolean TryGetOrdinal( this System.Data.IDataReader reader, IDbColumn column, out System.Int32 ordinal ) {
			ordinal = -1;
			if ( null == column ) {
				throw new System.ArgumentNullException( "column" );
			} else if ( ( null == reader ) || reader.IsClosed ) {
				throw new System.ArgumentNullException( "reader" );
			}
			System.Boolean output = false;
			try {
				var columnName = column.ColumnName;
				if ( System.String.IsNullOrEmpty( columnName ) ) {
					columnName = column.Name;
				}
				ordinal = reader.GetOrdinal( columnName );
				output = true;
			} catch ( System.Exception ) {
				;
			}
			return output;
		}

		// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DataReaderHelper"]/member[@name="GetValue(System.Data.IDataReader,Oits.Configuration.DbMap.IDbColumn)"]/*'/>
		public static System.Object GetValue( this System.Data.IDataReader reader, IDbColumn column ) {
			if ( ( null == reader ) || reader.IsClosed ) {
				throw new System.ArgumentNullException( "reader" );
			}
			var columnName = column.ColumnName;
			if ( System.String.IsNullOrEmpty( columnName ) ) {
				columnName = column.Name;
			}
			var ordinal = reader.GetOrdinal( columnName );
			if ( ( ordinal < 0 ) || ( reader.FieldCount <= ordinal ) ) {
				throw new System.ArgumentOutOfRangeException( "ordinal" );
			}

			return reader.GetValue( column, ordinal );
		}
		// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DataReaderHelper"]/member[@name="GetValue(System.Data.IDataReader,Oits.Configuration.DbMap.IDbColumn,System.Int32)"]/*'/>
		public static System.Object GetValue( this System.Data.IDataReader reader, IDbColumn column, System.Int32 ordinal ) {
			System.Object value = null;
			if ( column.IsNullable && reader.IsDBNull( ordinal ) ) {
				return value;
			} else if ( ( ordinal < 0 ) || ( reader.FieldCount <= ordinal ) ) {
				throw new System.ArgumentOutOfRangeException( "ordinal" );
			}

			var sqlDbType = column.SqlDbType;
			var dbType = column.DbType;
			var typeCode = column.TypeCode;
			var oleDbType = column.OleDbType;
			var odbcType = column.OdbcType;
			try {
				if ( System.Data.SqlDbType.Variant != sqlDbType ) {
					value = reader.GetValueBySqlDbType( sqlDbType, ordinal );
				} else if ( System.Data.DbType.Object != dbType ) {
					value = reader.GetValueByDbType( dbType, ordinal );
				} else if ( System.TypeCode.Object != typeCode ) {
					value = reader.GetValueByTypeCode( typeCode, ordinal );
				} else if ( System.Data.OleDb.OleDbType.Variant != oleDbType ) {
					value = reader.GetValueByOleDbType( oleDbType, ordinal );
				} else if ( 0 != (System.Int32)odbcType ) {
					value = reader.GetValueByOdbcType( odbcType, ordinal );
				} else {
					value = reader.GetValue( ordinal );
				}
			} catch ( System.Exception ex ) {
				ex.Data.Add( "ordinal", ordinal );
				ex.Data.Add( "column", column );
				throw;
			}

			if ( System.TypeCode.Object != column.ConvertTo ) {
				value = column.ConvertTo.ConvertValue( value );
			}

			return value;
		}

		// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DataReaderHelper"]/member[@name="GetValueByTypeCode(System.Data.IDataReader,System.TypeCode,System.Int32)"]/*'/>
		public static System.Object GetValueByTypeCode( this System.Data.IDataReader reader, System.TypeCode typeCode, System.Int32 ordinal ) {
			if ( ( null == reader ) || ( reader.IsClosed ) ) {
				throw new System.ArgumentNullException( "reader" );
			} else if ( ( ordinal < 0 ) || ( reader.FieldCount <= ordinal ) ) {
				throw new System.ArgumentOutOfRangeException( "ordinal" );
			}

			System.Object output = null;

			switch ( typeCode ) {
				case System.TypeCode.Object:
					output = reader.GetValue( ordinal );
					break;
				case System.TypeCode.Boolean:
					output = reader.GetBoolean( ordinal );
					break;
				case System.TypeCode.Byte:
					output = reader.GetByte( ordinal );
					break;
				case System.TypeCode.Char:
					output = reader.GetChar( ordinal );
					break;
				case System.TypeCode.DateTime:
					output = reader.GetDateTime( ordinal );
					break;
				case System.TypeCode.Decimal:
					output = reader.GetDecimal( ordinal );
					break;
				case System.TypeCode.Double:
					output = reader.GetDouble( ordinal );
					break;
				case System.TypeCode.Int16:
					output = reader.GetInt16( ordinal );
					break;
				case System.TypeCode.Int32:
					output = reader.GetInt32( ordinal );
					break;
				case System.TypeCode.Int64:
					output = reader.GetInt64( ordinal );
					break;
				case System.TypeCode.SByte:
					output = System.Convert.ToSByte( reader.GetValue( ordinal ) );
					break;
				case System.TypeCode.Single:
					output = reader.GetFloat( ordinal );
					break;
				case System.TypeCode.String:
					output = reader.GetString( ordinal );
					break;
				case System.TypeCode.UInt16:
					output = System.Convert.ToUInt16( reader.GetInt16( ordinal ) );
					break;
				case System.TypeCode.UInt32:
					output = System.Convert.ToUInt32( reader.GetInt32( ordinal ) );
					break;
				case System.TypeCode.UInt64:
					output = System.Convert.ToUInt64( reader.GetInt64( ordinal ) );
					break;
				default:
					throw new System.NotSupportedException();
			}

			return output;
		}
		// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DataReaderHelper"]/member[@name="GetValueByDbType(System.Data.IDataReader,System.Data.DbType,System.Int32)"]/*'/>
		public static System.Object GetValueByDbType( this System.Data.IDataReader reader, System.Data.DbType dbType, System.Int32 ordinal ) {
			if ( ( null == reader ) || ( reader.IsClosed ) ) {
				throw new System.ArgumentNullException( "reader" );
			} else if ( ( ordinal < 0 ) || ( reader.FieldCount <= ordinal ) ) {
				throw new System.ArgumentOutOfRangeException( "ordinal" );
			}

			System.Object output = null;

			switch ( dbType ) {
				case System.Data.DbType.Object:
					output = reader.GetValue( ordinal );
					break;
				case System.Data.DbType.AnsiString:
				case System.Data.DbType.AnsiStringFixedLength:
				case System.Data.DbType.String:
				case System.Data.DbType.StringFixedLength:
				case System.Data.DbType.Xml:
					output = reader.GetString( ordinal );
					break;
				case System.Data.DbType.Binary:
					output = ReadByteArray( reader, ordinal );
					break;
				case System.Data.DbType.Boolean:
					output = reader.GetBoolean( ordinal );
					break;
				case System.Data.DbType.Byte:
					output = reader.GetByte( ordinal );
					break;
				case System.Data.DbType.Currency:
				case System.Data.DbType.Decimal:
				case System.Data.DbType.VarNumeric:
					output = System.Convert.ToDecimal( reader.GetValue( ordinal ) );
					break;
				case System.Data.DbType.Date:
				case System.Data.DbType.DateTime:
				case System.Data.DbType.DateTime2:
					output = reader.GetDateTime( ordinal );
					break;
				case System.Data.DbType.DateTimeOffset:
					output = (System.DateTimeOffset)reader.GetValue( ordinal );
					break;
				case System.Data.DbType.Double:
					output = reader.GetDouble( ordinal );
					break;
				case System.Data.DbType.Guid:
					output = reader.GetGuid( ordinal );
					break;
				case System.Data.DbType.Int16:
					output = reader.GetInt16( ordinal );
					break;
				case System.Data.DbType.Int32:
					output = reader.GetInt32( ordinal );
					break;
				case System.Data.DbType.Int64:
					output = reader.GetInt64( ordinal );
					break;
				case System.Data.DbType.SByte:
					output = System.Convert.ToSByte( reader.GetValue( ordinal ) );
					break;
				case System.Data.DbType.Single:
					output = reader.GetFloat( ordinal );
					break;
				case System.Data.DbType.Time:
					output = (System.TimeSpan)reader.GetValue( ordinal );
					break;
				case System.Data.DbType.UInt16:
					output = System.Convert.ToUInt16( reader.GetInt16( ordinal ) );
					break;
				case System.Data.DbType.UInt32:
					output = System.Convert.ToUInt32( reader.GetInt32( ordinal ) );
					break;
				case System.Data.DbType.UInt64:
					output = System.Convert.ToUInt64( reader.GetInt64( ordinal ) );
					break;
				default:
					throw new System.NotSupportedException();
			}

			return output;
		}
		// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DataReaderHelper"]/member[@name="GetValueBySqlDbType(System.Data.IDataReader,System.Data.SqlDbType,System.Int32)"]/*'/>
		public static System.Object GetValueBySqlDbType( this System.Data.IDataReader reader, System.Data.SqlDbType sqlDbType, System.Int32 ordinal ) {
			if ( ( null == reader ) || ( reader.IsClosed ) ) {
				throw new System.ArgumentNullException( "reader" );
			} else if ( ( ordinal < 0 ) || ( reader.FieldCount <= ordinal ) ) {
				throw new System.ArgumentOutOfRangeException( "ordinal" );
			}

			var sqlReader = ( reader as System.Data.SqlClient.SqlDataReader );

			System.Object output = null;

			switch ( sqlDbType ) {
				case System.Data.SqlDbType.Structured:
				case System.Data.SqlDbType.Udt:
				case System.Data.SqlDbType.Variant:
					output = reader.GetValue( ordinal );
					break;

				case System.Data.SqlDbType.BigInt:
					output = reader.GetInt64( ordinal );
					break;

				case System.Data.SqlDbType.Binary:
				case System.Data.SqlDbType.Image:
				case System.Data.SqlDbType.Timestamp:
				case System.Data.SqlDbType.VarBinary:
					output = ReadByteArray( reader, ordinal );
					break;

				case System.Data.SqlDbType.Bit:
					output = reader.GetBoolean( ordinal );
					break;

				case System.Data.SqlDbType.Char:
				case System.Data.SqlDbType.NChar:
				case System.Data.SqlDbType.NText:
				case System.Data.SqlDbType.NVarChar:
				case System.Data.SqlDbType.Text:
				case System.Data.SqlDbType.VarChar:
					output = reader.GetString( ordinal );
					break;

				case System.Data.SqlDbType.Date:
				case System.Data.SqlDbType.DateTime:
				case System.Data.SqlDbType.DateTime2:
				case System.Data.SqlDbType.SmallDateTime:
					output = reader.GetDateTime( ordinal );
					break;

				case System.Data.SqlDbType.DateTimeOffset:
					if ( null != sqlReader ) {
						output = sqlReader.GetDateTimeOffset( ordinal );
					} else {
						output = (System.DateTimeOffset)reader.GetValue( ordinal );
					}
					break;

				case System.Data.SqlDbType.Decimal:
				case System.Data.SqlDbType.Money:
				case System.Data.SqlDbType.SmallMoney:
					output = reader.GetDecimal( ordinal );
					break;

				case System.Data.SqlDbType.Float:
					output = reader.GetDouble( ordinal );
					break;

				case System.Data.SqlDbType.Int:
					output = reader.GetInt32( ordinal );
					break;

				case System.Data.SqlDbType.Real:
					output = reader.GetFloat( ordinal );
					break;

				case System.Data.SqlDbType.SmallInt:
					output = reader.GetInt16( ordinal );
					break;

				case System.Data.SqlDbType.Time:
					if ( null != sqlReader ) {
						output = sqlReader.GetTimeSpan( ordinal );
					} else {
						output = (System.TimeSpan)reader.GetValue( ordinal );
					}
					break;

				case System.Data.SqlDbType.TinyInt:
					output = reader.GetByte( ordinal );
					break;

				case System.Data.SqlDbType.UniqueIdentifier:
					output = reader.GetGuid( ordinal );
					break;

				case System.Data.SqlDbType.Xml:
					if ( null == sqlReader ) {
						throw new System.NotSupportedException( sqlDbType.ToString() );
					}
					var xml = sqlReader.GetSqlXml( ordinal );
					if ( ( null == xml ) || ( xml.IsNull ) ) {
						output = null;
					} else {
						output = xml.Value;
					}
					break;

				default:
					throw new System.NotSupportedException( sqlDbType.ToString() );
			}

			return output;
		}
		// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DataReaderHelper"]/member[@name="GetValueByOleDbType(System.Data.IDataReader,System.Data.OleDb.OleDbType,System.Int32)"]/*'/>
		public static System.Object GetValueByOleDbType( this System.Data.IDataReader reader, System.Data.OleDb.OleDbType oleDbType, System.Int32 ordinal ) {
			if ( ( null == reader ) || ( reader.IsClosed ) ) {
				throw new System.ArgumentNullException( "reader" );
			} else if ( ( ordinal < 0 ) || ( reader.FieldCount <= ordinal ) ) {
				throw new System.ArgumentOutOfRangeException( "ordinal" );
			}

			var oleDbReader = ( reader as System.Data.OleDb.OleDbDataReader );

			System.Object output = null;

			switch ( oleDbType ) {
				case System.Data.OleDb.OleDbType.Variant:
				case System.Data.OleDb.OleDbType.PropVariant:
					output = reader.GetValue( ordinal );
					break;
				case System.Data.OleDb.OleDbType.BigInt:
					output = reader.GetInt64( ordinal );
					break;
				case System.Data.OleDb.OleDbType.Binary:
				case System.Data.OleDb.OleDbType.LongVarBinary:
				case System.Data.OleDb.OleDbType.VarBinary:
					output = ReadByteArray( reader, ordinal );
					break;
				case System.Data.OleDb.OleDbType.Boolean:
					output = reader.GetBoolean( ordinal );
					break;
				case System.Data.OleDb.OleDbType.BSTR:
				case System.Data.OleDb.OleDbType.Char:
				case System.Data.OleDb.OleDbType.LongVarChar:
				case System.Data.OleDb.OleDbType.LongVarWChar:
				case System.Data.OleDb.OleDbType.VarChar:
				case System.Data.OleDb.OleDbType.VarWChar:
				case System.Data.OleDb.OleDbType.WChar:
					output = reader.GetString( ordinal );
					break;
				case System.Data.OleDb.OleDbType.Currency:
				case System.Data.OleDb.OleDbType.Decimal:
				case System.Data.OleDb.OleDbType.Numeric:
				case System.Data.OleDb.OleDbType.VarNumeric:
					output = reader.GetDecimal( ordinal );
					break;
				case System.Data.OleDb.OleDbType.Date:
				case System.Data.OleDb.OleDbType.DBDate:
				case System.Data.OleDb.OleDbType.DBTimeStamp:
				case System.Data.OleDb.OleDbType.Filetime:
					output = reader.GetDateTime( ordinal );
					break;
				case System.Data.OleDb.OleDbType.DBTime:
					if ( null != reader ) {
						output = oleDbReader.GetTimeSpan( ordinal );
					} else {
						output = (System.TimeSpan)reader.GetValue( ordinal );
					}
					break;
				case System.Data.OleDb.OleDbType.Double:
					output = reader.GetDouble( ordinal );
					break;
				case System.Data.OleDb.OleDbType.Guid:
					output = reader.GetGuid( ordinal );
					break;
				case System.Data.OleDb.OleDbType.Integer:
					output = reader.GetInt32( ordinal );
					break;
				case System.Data.OleDb.OleDbType.Single:
					output = reader.GetFloat( ordinal );
					break;
				case System.Data.OleDb.OleDbType.SmallInt:
					output = reader.GetInt16( ordinal );
					break;
				case System.Data.OleDb.OleDbType.TinyInt:
					output = System.Convert.ToSByte( reader.GetValue( ordinal ) );
					break;
				case System.Data.OleDb.OleDbType.UnsignedBigInt:
					output = System.Convert.ToUInt64( reader.GetValue( ordinal ) );
					break;
				case System.Data.OleDb.OleDbType.UnsignedInt:
					output = System.Convert.ToUInt32( reader.GetValue( ordinal ) );
					break;
				case System.Data.OleDb.OleDbType.UnsignedSmallInt:
					output = System.Convert.ToUInt16( reader.GetValue( ordinal ) );
					break;
				case System.Data.OleDb.OleDbType.UnsignedTinyInt:
					output = reader.GetByte( ordinal );
					break;
				default:
					throw new System.NotSupportedException( oleDbType.ToString() );
			}

			return output;
		}
		// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DataReaderHelper"]/member[@name="GetValueByOdbcType(System.Data.IDataReader,System.Data.Odbc.OdbcType,System.Int32)"]/*'/>
		public static System.Object GetValueByOdbcType( this System.Data.IDataReader reader, System.Data.Odbc.OdbcType odbcType, System.Int32 ordinal ) {
			if ( ( null == reader ) || ( reader.IsClosed ) ) {
				throw new System.ArgumentNullException( "reader" );
			} else if ( ( ordinal < 0 ) || ( reader.FieldCount <= ordinal ) ) {
				throw new System.ArgumentOutOfRangeException( "ordinal" );
			}

			var odbcReader = ( reader as System.Data.Odbc.OdbcDataReader );

			System.Object output = null;

			switch ( odbcType ) {
				case System.Data.Odbc.OdbcType.BigInt:
					output = reader.GetInt64( ordinal );
					break;
				case System.Data.Odbc.OdbcType.Binary:
				case System.Data.Odbc.OdbcType.Image:
				case System.Data.Odbc.OdbcType.Timestamp:
				case System.Data.Odbc.OdbcType.VarBinary:
					output = ReadByteArray( reader, ordinal );
					break;
				case System.Data.Odbc.OdbcType.Bit:
					output = reader.GetBoolean( ordinal );
					break;
				case System.Data.Odbc.OdbcType.Char:
				case System.Data.Odbc.OdbcType.NChar:
				case System.Data.Odbc.OdbcType.NText:
				case System.Data.Odbc.OdbcType.NVarChar:
				case System.Data.Odbc.OdbcType.Text:
				case System.Data.Odbc.OdbcType.VarChar:
					output = reader.GetString( ordinal );
					break;
				case System.Data.Odbc.OdbcType.Date:
				case System.Data.Odbc.OdbcType.DateTime:
				case System.Data.Odbc.OdbcType.SmallDateTime:
					output = reader.GetDateTime( ordinal );
					break;
				case System.Data.Odbc.OdbcType.Decimal:
				case System.Data.Odbc.OdbcType.Numeric:
					output = reader.GetDecimal( ordinal );
					break;
				case System.Data.Odbc.OdbcType.Double:
					output = reader.GetDouble( ordinal );
					break;
				case System.Data.Odbc.OdbcType.Int:
					output = reader.GetInt32( ordinal );
					break;
				case System.Data.Odbc.OdbcType.Real:
					output = reader.GetFloat( ordinal );
					break;
				case System.Data.Odbc.OdbcType.SmallInt:
					output = reader.GetInt16( ordinal );
					break;
				case System.Data.Odbc.OdbcType.Time:
					if ( null != odbcReader ) {
						output = odbcReader.GetTime( ordinal );
					} else {
						output = (System.TimeSpan)reader.GetValue( ordinal );
					}
					break;
				case System.Data.Odbc.OdbcType.TinyInt:
					output = reader.GetByte( ordinal );
					break;
				case System.Data.Odbc.OdbcType.UniqueIdentifier:
					output = reader.GetGuid( ordinal );
					break;
				default:
					throw new System.NotSupportedException( odbcType.ToString() );
			}

			return output;
		}
		// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DataReaderHelper"]/member[@name="ReadByteArray(System.Data.IDataReader,System.Int32)"]/*'/>
		public static System.Byte[] ReadByteArray( this System.Data.IDataRecord reader, System.Int32 ordinal ) {
			using ( var store = new System.IO.MemoryStream() ) {
				System.Byte[] buffer = new System.Byte[ BufferLength ];
				System.Int64 p = 0;
				System.Int64 r = reader.GetBytes( ordinal, p, buffer, 0, BufferLength );
				while ( 0 < r ) {
					store.Write( buffer, 0, (System.Int32)r );
					p += r;
					r = reader.GetBytes( ordinal, p, buffer, 0, BufferLength );
				}
				return store.ToArray();
			}
		}
		// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DataReaderHelper"]/member[@name="ReadCharArray(System.Data.IDataReader,System.Int32)"]/*'/>
		public static System.String ReadCharArray( this System.Data.IDataRecord reader, System.Int32 ordinal ) {
			var store = new System.Text.StringBuilder();
			System.Char[] buffer = new System.Char[ BufferLength ];
			System.Int64 p = 0;
			System.Int64 r = reader.GetChars( ordinal, p, buffer, 0, BufferLength );
			while ( 0 < r ) {
				store.Append( buffer, 0, (System.Int32)r );
				p += r;
				r = reader.GetChars( ordinal, p, buffer, 0, BufferLength );
			}
			return store.ToString();
		}
		// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DataReaderHelper"]/member[@name="ConvertValue(System.TypeCode,System.Int32)"]/*'/>
		public static System.Object ConvertValue( this System.TypeCode convertTo, System.Object value ) {
			System.Object output = value;

			switch ( convertTo ) {
				case System.TypeCode.Object:
					break;
				case System.TypeCode.Boolean:
					output = System.Convert.ToBoolean( value );
					break;
				case System.TypeCode.Byte:
					output = System.Convert.ToByte( value );
					break;
				case System.TypeCode.Char:
					output = System.Convert.ToChar( value );
					break;
				case System.TypeCode.DateTime:
					output = System.Convert.ToDateTime( value );
					break;
				case System.TypeCode.Decimal:
					output = System.Convert.ToDecimal( value );
					break;
				case System.TypeCode.Double:
					output = System.Convert.ToDouble( value );
					break;
				case System.TypeCode.Int16:
					output = System.Convert.ToInt16( value );
					break;
				case System.TypeCode.Int32:
					output = System.Convert.ToInt32( value );
					break;
				case System.TypeCode.Int64:
					output = System.Convert.ToInt64( value );
					break;
				case System.TypeCode.SByte:
					output = System.Convert.ToSByte( value );
					break;
				case System.TypeCode.Single:
					output = System.Convert.ToSingle( value );
					break;
				case System.TypeCode.String:
					output = System.Convert.ToString( value );
					break;
				case System.TypeCode.UInt16:
					output = System.Convert.ToUInt16( value );
					break;
				case System.TypeCode.UInt32:
					output = System.Convert.ToUInt32( value );
					break;
				case System.TypeCode.UInt64:
					output = System.Convert.ToUInt64( value );
					break;
				default:
					throw new System.NotSupportedException();
			}

			return output;
		}

	}

}