﻿namespace Oits.Configuration.DbMap {

	/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbMapSection"]/member[@name=""]/*'/>
	[System.Serializable]
	public class DbMapSection : System.Configuration.ConfigurationSection { 

		#region fields
		/// <summary>The default name of the <see cref="Oits.Configuration.DbMap.DbMapSection" /> element in the configuration file.</summary>
		public const System.String DefaultSectionName = "oits.dbmap";
		#endregion fields


		#region .ctor
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbMapSection"]/member[@name=".#ctor"]/*'/>
		public DbMapSection() : base() { 
		}
		#endregion .ctor


		#region properties
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbMapSection"]/member[@name="Groups"]/*'/>
		[System.Configuration.ConfigurationProperty( "", IsDefaultCollection = true, IsRequired = false )]
		[System.Configuration.ConfigurationCollection( typeof( DbMapGroupCollection ), 
			AddItemName = "add", 
			ClearItemsName = "clear", 
			RemoveItemName = "remove" 
		)]
		public DbMapGroupCollection Groups { 
			get {
				return (DbMapGroupCollection)base[ "" ];
			}
		}
		#endregion properties


		#region static methods
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbMapSection"]/member[@name="GetSection()"]/*'/>
		public static DbMapSection GetSection() { 
			return ( System.Configuration.ConfigurationManager.GetSection( Oits.Configuration.DbMap.DbMapSection.DefaultSectionName ) as DbMapSection );
		}
		#endregion static methods

	}

}