﻿using System.Linq;

namespace Oits.Configuration.DbMap {

	/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbEventArgs"]/member[@name=""]/*'/>
	[System.Serializable]
	public class DbEventArgs : System.EventArgs {

		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbEventArgs"]/member[@name=".#ctor"]/*'/>
		public DbEventArgs() : base() {
		}

	}

}