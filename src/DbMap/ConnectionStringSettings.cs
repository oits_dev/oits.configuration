﻿using System.Linq;

namespace Oits.Configuration.DbMap {

	/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.ConnectionStringSettings"]/member[@name=""]/*'/>
	public static class ConnectionStringSettings {

		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.ConnectionStringSettings"]/member[@name="GetConnection(System.Configuration.ConnectionStringSettings)"]/*'/>
		public static System.Data.Common.DbConnection GetConnection( this System.Configuration.ConnectionStringSettings settings ) {
			if ( null == settings ) {
				throw new System.ArgumentNullException( "settings" );
			}
			return GetConnection( settings.ConnectionString, settings.ProviderName );
		}

		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.ConnectionStringSettings"]/member[@name="GetConnection(System.String,System.String)"]/*'/>
		public static System.Data.Common.DbConnection GetConnection( this System.String connectionString, System.String providerName ) {
			if ( System.String.IsNullOrEmpty( connectionString ) ) {
				throw new System.ArgumentNullException( "connectionString" );
			}
			var cnxn = System.Data.Common.DbProviderFactories.GetFactory( providerName ).CreateConnection();
			cnxn.ConnectionString = connectionString;
			return cnxn;
		}

	}

}