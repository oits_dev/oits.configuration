﻿using System.Linq;

namespace Oits.Configuration.DbMap { 

	/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbResultCollection"]/member[@name=""]/*'/>
	[System.Serializable]
	public class DbResultCollection : NamedConfigurationElementCollectionBase<DbResultElement> { 

		#region .ctor
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbResultCollection"]/member[@name=".#ctor"]/*'/>
		public DbResultCollection() : base() { 
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbResultCollection"]/member[@name=".#ctor(System.Collections.IComparer)"]/*'/>
		public DbResultCollection( System.Collections.IComparer comparer ) : base( comparer ) { 
		}
		#endregion .ctor

	}

}