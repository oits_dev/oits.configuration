﻿using System.Linq;

namespace Oits.Configuration.DbMap {

	/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbColumnCollection"]/member[@name=""]/*'/>
	[System.Serializable]
	public class DbColumnCollection : NamedConfigurationElementCollectionBase<DbColumnElement> {

		#region .ctor
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbColumnCollection"]/member[@name=".#ctor"]/*'/>
		public DbColumnCollection() : base() {
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbColumnCollection"]/member[@name=".#ctor(System.Collections.IComparer)"]/*'/>
		public DbColumnCollection( System.Collections.IComparer comparer ) : base( comparer ) {
		}
		#endregion .ctor


		#region methods
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbColumnCollection"]/member[@name="GetOrdinals(System.Data.Common.DbDataReader)"]/*'/>
		public System.Collections.Generic.IDictionary<DbColumnElement, System.Int32> GetOrdinals( System.Data.Common.DbDataReader reader ) {
			if ( null == reader ) {
				throw new System.ArgumentNullException( "reader" );
			}

			var map = new System.Collections.Generic.Dictionary<DbColumnElement, System.Int32>( System.Math.Max( 1, this.Count ) );
			System.Int32 ordinal;
			foreach ( var c in this.OfType<DbColumnElement>().Where(
				x => ( null != x )
			) ) {
				if ( c.IsRequired ) {
					map.Add( c, c.GetOrdinal( reader ) );
				} else {
					if ( c.TryGetOrdinal( reader, out ordinal ) ) {
						map.Add( c, ordinal );
					}
				}
			}
			return map;
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbColumnCollection"]/member[@name="GetProperties(System.Type)"]/*'/>
		public System.Collections.Generic.IDictionary<DbColumnElement, System.Reflection.PropertyInfo> GetProperties( System.Type type ) {
			if ( null == type ) {
				throw new System.ArgumentNullException( "type" );
			}

			var map = new System.Collections.Generic.Dictionary<DbColumnElement, System.Reflection.PropertyInfo>( System.Math.Max( 1, this.Count ) );
			System.Reflection.PropertyInfo pi;
			foreach ( var c in this.OfType<DbColumnElement>().Where(
				x => ( null != x )
			) ) {
				if ( c.TryGetProperty( type, out pi ) ) {
					map.Add( c, pi );
				}
			}
			return map;
		}

		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbColumnCollection"]/member[@name="SetPropertyValues(System.Data.Common.DbDataReader,System.Object)"]/*'/>
		public void SetPropertyValues( System.Data.Common.DbDataReader reader, System.Object @object ) {
			if ( null == @object ) {
				throw new System.ArgumentNullException( "object" );
			} else if ( ( null == reader ) || reader.IsClosed ) {
				throw new System.ArgumentNullException( "reader" );
			}

			if ( reader.HasRows ) {
				this.SetPropertyValues( reader, this.GetOrdinals( reader ), this.GetProperties( @object.GetType() ), @object );
			}
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbColumnCollection"]/member[@name="SetPropertyValues(System.Data.Common.DbDataReader,System.Collections.Generic.IDictionary`2,System.Collections.Generic.IDictionary`2,System.Object)"]/*'/>
		public void SetPropertyValues( System.Data.Common.DbDataReader reader, System.Collections.Generic.IDictionary<DbColumnElement, System.Int32> ordinals, System.Collections.Generic.IDictionary<DbColumnElement, System.Reflection.PropertyInfo> properties, System.Object @object ) {
			if ( null == @object ) {
				throw new System.ArgumentNullException( "object" );
			} else if ( ( null == properties ) || ( properties.Count <= 0 ) ) {
				throw new System.ArgumentNullException( "properties" );
			} else if ( ( null == ordinals ) || ( ordinals.Count <= 0 ) ) {
				throw new System.ArgumentNullException( "ordinals" );
			} else if ( ( null == reader ) || ( reader.IsClosed ) ) {
				throw new System.ArgumentNullException( "reader" );
			}

			if ( reader.HasRows ) {
				System.Int32 fc = reader.FieldCount;
				System.Int32 o;
				DbColumnElement key;
				foreach ( var kvp in properties.Where<System.Collections.Generic.KeyValuePair<DbColumnElement, System.Reflection.PropertyInfo>>(
					x => {
						if ( null == x.Value ) {
							return false;
						}
						var k = x.Key;
						if ( k.Skip ) {
							return false;
						}
						if ( !ordinals.ContainsKey( k ) ) {
							return false;
						}
						o = ordinals[ k ];
						return (
							( 0 <= o ) && ( o < fc )
						);
					}
				) ) {
					key = kvp.Key;
					key.SetPropertyValue( reader, ordinals[ kvp.Key ], kvp.Value, @object );
				}
			}
		}
		#endregion methods

	}

}