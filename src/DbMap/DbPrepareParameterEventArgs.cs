﻿using System.Linq;

namespace Oits.Configuration.DbMap {

	/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbPrepareParameterEventArgs"]/member[@name=""]/*'/>
	[System.Serializable]
	public class DbPrepareParameterEventArgs : DbParameterEventArgs {

		#region .ctor
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbPrepareParameterEventArgs"]/member[@name=".#ctor"]/*'/>
		public DbPrepareParameterEventArgs() : base() {
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbPrepareParameterEventArgs"]/member[@name=".#ctor(System.Data.Common.DbCommand,System.Data.Common.DbParameter,DbMap.DbParameterElement)"]/*'/>
		public DbPrepareParameterEventArgs( System.Data.Common.DbCommand command, System.Data.Common.DbParameter parameter, DbMap.DbParameterElement element ) : base( command, parameter, element ) {
		}
		#endregion.ctor

	}

}