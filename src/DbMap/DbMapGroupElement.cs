﻿using System.Linq;
using Oits.Configuration.DbMap;

namespace Oits.Configuration.DbMap {

	/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbMapGroupElement"]/member[@name=""]/*'/>
	[System.Serializable]
	public class DbMapGroupElement : System.Configuration.ConfigurationElement, INamedConfigurationElement { 

		#region .ctor
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbMapGroupElement"]/member[@name=".#ctor"]/*'/>
		public DbMapGroupElement() : base() { 
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbMapGroupElement"]/member[@name=".#ctor(System.String)"]/*'/>
		public DbMapGroupElement( System.String name ) : this() { 
			this.Name = name;
		}
		#endregion .ctor


		#region properties
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbMapGroupElement"]/member[@name="Name"]/*'/>
		[System.Configuration.ConfigurationProperty( "name", IsRequired = true, IsKey = true )]
		public System.String Name { 
			get { 
				return (System.String)base[ "name" ];
			}
			set { 
				base[ "name" ] = value;
			}
		}

		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbMapGroupElement"]/member[@name="ConnectionStringName"]/*'/>
		[System.Configuration.ConfigurationProperty( "connectionStringName", IsRequired = false )]
		public System.String ConnectionStringName { 
			get { 
				return (System.String)base[ "connectionStringName" ];
			}
			set { 
				base[ "connectionStringName" ] = value;
			}
		}

		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbMapGroupElement"]/member[@name="Commands"]/*'/>
		[System.Configuration.ConfigurationProperty( "commands", IsDefaultCollection = false, IsRequired = false )]
		[System.Configuration.ConfigurationCollection( typeof( DbCommandCollection ), 
			AddItemName = "add", 
			ClearItemsName = "clear", 
			RemoveItemName = "remove" 
		)]
		public DbCommandCollection Commands { 
			get { 
				return (DbCommandCollection)base[ "commands" ];
			}
		}

		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbMapGroupElement"]/member[@name="Results"]/*'/>
		[System.Configuration.ConfigurationProperty( "results", IsDefaultCollection = false, IsRequired = false )]
		[System.Configuration.ConfigurationCollection( typeof( DbResultCollection ),
			AddItemName = "add",
			ClearItemsName = "clear",
			RemoveItemName = "remove"
		)]
		public DbResultCollection Results {
			get {
				return (DbResultCollection)base[ "results" ];
			}
		}
		#endregion properties


		#region methods
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbMapGroupElement"]/member[@name="CreateConnection"]/*'/>
		public System.Data.Common.DbConnection CreateConnection() { 
			return CreateConnection( this.ConnectionStringName );
		}
		#endregion methods


		#region static methods
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbMapGroupElement"]/member[@name="CreateConnection(System.String)"]/*'/>
		public static System.Data.Common.DbConnection CreateConnection( System.String connectionStringName ) { 
			if ( System.String.IsNullOrEmpty( connectionStringName ) ) { 
				throw new System.ArgumentNullException( "connectionStringName" );
			}

			var settings = System.Configuration.ConfigurationManager.ConnectionStrings[ connectionStringName ];
			if ( null == settings ) { 
				throw new System.InvalidOperationException();
			}

			return settings.GetConnection();
		}
		#endregion static methods

	}

}