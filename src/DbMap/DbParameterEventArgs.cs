﻿using System.Linq;

namespace Oits.Configuration.DbMap {

	/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbParameterEventArgs"]/member[@name=""]/*'/>
	[System.Serializable]
	public class DbParameterEventArgs : DbEventArgs {

		#region fields
		private readonly System.Data.Common.DbCommand myCommand;
		private readonly System.Data.Common.DbParameter myParameter;
		private readonly DbMap.DbParameterElement myElement;
		#endregion fields


		#region .ctor
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbParameterEventArgs"]/member[@name=".#ctor"]/*'/>
		public DbParameterEventArgs() : base() {
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbParameterEventArgs"]/member[@name=".#ctor(System.Data.Common.DbCommand,System.Data.Common.DbParameter,DbMap.DbParameterElement)"]/*'/>
		public DbParameterEventArgs( System.Data.Common.DbCommand command, System.Data.Common.DbParameter parameter, DbMap.DbParameterElement element ) : this() {
			myCommand = command;
			myParameter = parameter;
			myElement = element;
		}
		#endregion .ctor


		#region properties
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbParameterEventArgs"]/member[@name="Command"]/*'/>
		public System.Data.Common.DbCommand Command {
			get {
				return myCommand;
			}
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbParameterEventArgs"]/member[@name="Parameter"]/*'/>
		public System.Data.Common.DbParameter Parameter {
			get {
				return myParameter;
			}
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbParameterEventArgs"]/member[@name="Element"]/*'/>
		public DbMap.DbParameterElement Element {
			get {
				return myElement;
			}
		}
		#endregion properties

	}

}