﻿namespace Oits.Configuration.DbMap { 

	/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.ReturnValueBase"]/member[@name=""]/*'/>
	[System.Serializable]
	public class ReturnValueBase {

		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.ReturnValueBase"]/member[@name=".#ctor"]/*'/>
		public ReturnValueBase() : base() { 
		}

		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.ReturnValueBase"]/member[@name="ReturnValue"]/*'/>
		public System.Int32 ReturnValue { 
			get;
			set;
		}

	}

}