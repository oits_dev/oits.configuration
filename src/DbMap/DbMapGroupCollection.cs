﻿using System.Linq;

namespace Oits.Configuration.DbMap {

	/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbMapGroupCollection"]/member[@name=""]/*'/>
	[System.Serializable]
	public class DbMapGroupCollection : NamedConfigurationElementCollectionBase<DbMapGroupElement> { 

		#region .ctor
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbMapGroupCollection"]/member[@name=".#ctor"]/*'/>
		public DbMapGroupCollection() : base() { 
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.DbMap.DbMapGroupCollection"]/member[@name=".#ctor(System.Collections.IComparer)"]/*'/>
		public DbMapGroupCollection( System.Collections.IComparer comparer ) : base( comparer ) { 
		}
		#endregion .ctor

	}

}