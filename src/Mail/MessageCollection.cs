﻿namespace Oits.Configuration.Mail { 

	/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Mail.MessageCollection"]/member[@name=""]/*'/>
	[System.Serializable]
	public class MessageCollection : NamedConfigurationElementCollectionBase<MessageElement> { 

		#region .ctor
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Mail.MessageCollection"]/member[@name=".#ctor"]/*'/>
		public MessageCollection() : base() { 
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Mail.MessageCollection"]/member[@name=".#ctor(System.Collections.IComparer)"]/*'/>
		public MessageCollection( System.Collections.IComparer comparer ) : base( comparer ) { 
		}
		#endregion .ctor

	}

}