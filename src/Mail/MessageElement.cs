﻿namespace Oits.Configuration.Mail {

	/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Mail.MessageElement"]/member[@name=""]/*'/>
	[System.Serializable]
	public class MessageElement : System.Configuration.ConfigurationElement, INamedConfigurationElement { 

		#region .ctor
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Mail.MessageElement"]/member[@name=".#ctor"]/*'/>
		public MessageElement() :  base() { 
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Mail.MessageElement"]/member[@name=".#ctor(System.String)"]/*'/>
		public MessageElement( System.String name ) : this() { 
			this.Name = name;
		}
		#endregion .ctor


		#region properties
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Mail.MessageElement"]/member[@name="Name"]/*'/>
		[System.Configuration.ConfigurationProperty( "name", DefaultValue = (System.String)null, IsRequired = true, IsKey = true )]
		public System.String Name { 
			get { 
				return (System.String)this[ "name" ];
			}
			set { 
				this[ "name" ] = value;
			}
		}

		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Mail.MessageElement"]/member[@name="Subject"]/*'/>
		[System.Configuration.ConfigurationProperty( "subject", DefaultValue = (System.String)null, IsRequired = false, IsKey = false )]
		public System.String Subject {
			get {
				return (System.String)this[ "subject" ];
			}
			set {
				this[ "subject" ] = value;
			}
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Mail.MessageElement"]/member[@name="SubjectFormat"]/*'/>
		[System.Configuration.ConfigurationProperty( "subjectFormat", DefaultValue = (System.String)null, IsRequired = false, IsKey = false )]
		public System.String SubjectFormat {
			get {
				return (System.String)this[ "subjectFormat" ];
			}
			set {
				this[ "subjectFormat" ] = value;
			}
		}

		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Mail.MessageElement"]/member[@name="Body"]/*'/>
		[System.Configuration.ConfigurationProperty( "body", DefaultValue = (System.String)null, IsRequired = false, IsKey = false )]
		public System.String Body {
			get {
				return (System.String)this[ "body" ];
			}
			set {
				this[ "body" ] = value;
			}
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Mail.MessageElement"]/member[@name="BodyFormat"]/*'/>
		[System.Configuration.ConfigurationProperty( "bodyFormat", DefaultValue = (System.String)null, IsRequired = false, IsKey = false )]
		public System.String BodyFormat {
			get {
				return (System.String)this[ "bodyFormat" ];
			}
			set {
				this[ "bodyFormat" ] = value;
			}
		}

		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Mail.MessageElement"]/member[@name="From"]/*'/>
		[System.Configuration.ConfigurationProperty( "from", DefaultValue = (AddressElement)null, IsRequired = false, IsKey = false )]
		public AddressElement From { 
			get { 
				return (AddressElement)this[ "from" ];
			}
			set { 
				this[ "from" ] = value;
			}
		}

		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Mail.MessageElement"]/member[@name="From"]/*'/>
		[System.Configuration.ConfigurationProperty( "to", DefaultValue = (AddressElement)null, IsRequired = false, IsKey = false )]
		public AddressCollection To { 
			get { 
				return (AddressCollection)this[ "to" ];
			}
			set { 
				this[ "to" ] = value;
			}
		}
		#endregion properties


		#region methods
		private System.String BuildSubject() { 
			var subject = this.SubjectFormat;
			if ( System.String.IsNullOrEmpty( subject ) ) { 
				subject = this.Subject;
			}
			return subject;
		}
		private System.String BuildSubject( params System.Object[] args ) { 
			var subject = this.BuildSubject();
			if ( ( null == args ) || ( 0 == args.Length ) ) { 
				return subject;
			} else { 
				return System.String.Format( subject, args );
			}
		}
		private System.String BuildBody() { 
			var body = this.BodyFormat;
			if ( System.String.IsNullOrEmpty( body ) ) { 
				body = this.Body;
			}
			return body;
		}
		private System.String BuildBody( params System.Object[] args ) { 
			var body = this.BuildBody();
			if ( ( null == args ) || ( 0 == args.Length ) ) { 
				return body;
			} else { 
				return System.String.Format( body, args );
			}
		}
		private System.Net.Mail.MailMessage BuildMessage() { 
			System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
			msg.Subject = this.BuildSubject();
			msg.Body = this.BuildBody();
			foreach ( var addy in ( this.To ?? new AddressCollection() ).ToMailAddressCollection() ) { 
				msg.To.Add( addy );
			}
			return msg;
		}
		private System.Net.Mail.MailMessage BuildMessage( params System.Object[] args ) { 
			System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
			msg.Subject = this.BuildSubject( args );
			msg.Body = this.BuildBody( args );
			foreach ( var addy in ( this.To ?? new AddressCollection() ).ToMailAddressCollection() ) { 
				msg.To.Add( addy );
			}
			return msg;
		}
		private System.Net.Mail.MailMessage BuildMessage( System.Net.Mail.MailAddressCollection collection ) { 
			System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
			msg.Subject = this.BuildSubject();
			msg.Body = this.BuildBody();
			foreach ( var addy in collection ) { 
				msg.To.Add( addy );
			}
			return msg;
		}
		private System.Net.Mail.MailMessage BuildMessage( System.Collections.Generic.IEnumerable<System.Net.Mail.MailAddress> collection ) { 
			System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
			msg.Subject = this.BuildSubject();
			msg.Body = this.BuildBody();
			foreach ( var addy in collection ) { 
				msg.To.Add( addy );
			}
			return msg;
		}
		private System.Net.Mail.MailMessage BuildMessage( System.Collections.Generic.IEnumerable<System.String> collection ) { 
			System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
			msg.Subject = this.BuildSubject();
			msg.Body = this.BuildBody();
			foreach ( var addy in collection ) { 
				msg.To.Add( addy );
			}
			return msg;
		}

		private System.Net.Mail.MailMessage BuildMessage( System.Net.Mail.MailAddressCollection collection, params System.Object[] args ) { 
			System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
			msg.Subject = this.BuildSubject( args );
			msg.Body = this.BuildBody( args );
			foreach ( var addy in collection ) { 
				msg.To.Add( addy );
			}
			return msg;
		}
		private System.Net.Mail.MailMessage BuildMessage( System.Collections.Generic.IEnumerable<System.Net.Mail.MailAddress> collection, params System.Object[] args ) { 
			System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
			msg.Subject = this.BuildSubject( args );
			msg.Body = this.BuildBody( args );
			foreach ( var addy in collection ) { 
				msg.To.Add( addy );
			}
			return msg;
		}
		private System.Net.Mail.MailMessage BuildMessage( System.Collections.Generic.IEnumerable<System.String> collection, params System.Object[] args ) { 
			System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
			msg.Subject = this.BuildSubject( args );
			msg.Body = this.BuildBody( args );
			foreach ( var addy in collection ) { 
				msg.To.Add( addy );
			}
			return msg;
		}

		private void SendMail( System.Net.Mail.MailMessage message ) { 
			if ( null == message ) { 
				throw new System.ArgumentNullException( "message" );
			}
			System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient();
			try { 
				client.Send( message );
			} catch ( System.Exception ) { 
				throw;
			} finally { 
				var q = ( client as System.IDisposable );
				if ( null != q ) { 
					q.Dispose();
				}
			}
		}

		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Mail.MessageElement"]/member[@name="Send()"]/*'/>
		public void Send() { 
			this.SendMail( this.BuildMessage() );
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Mail.MessageElement"]/member[@name="Send(System.Object[])"]/*'/>
		public void Send( params System.Object[] args ) {
			this.SendMail( this.BuildMessage( args ) );
		}

		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Mail.MessageElement"]/member[@name="Send(System.Net.Mail.MailAddressCollection)"]/*'/>
		public void Send( System.Net.Mail.MailAddressCollection collection ) { 
			this.SendMail( this.BuildMessage( collection ) );
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Mail.MessageElement"]/member[@name="Send(System.Net.Mail.MailAddressCollection,System.Object[])"]/*'/>
		public void Send( System.Net.Mail.MailAddressCollection collection, params System.Object[] args ) { 
			this.SendMail( this.BuildMessage( collection, args ) );
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Mail.MessageElement"]/member[@name="Send(System.Collections.Generic.IEnumerable{System.Net.Mail.MailAddress})"]/*'/>
		public void Send( System.Collections.Generic.IEnumerable<System.Net.Mail.MailAddress> collection ) { 
			this.SendMail( this.BuildMessage( collection ) );
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Mail.MessageElement"]/member[@name="Send(System.Collections.Generic.IEnumerable{System.Net.Mail.MailAddress},System.Object[])"]/*'/>
		public void Send( System.Collections.Generic.IEnumerable<System.Net.Mail.MailAddress> collection, params System.Object[] args ) { 
			this.SendMail( this.BuildMessage( collection, args ) );
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Mail.MessageElement"]/member[@name="Send(System.Collections.Generic.IEnumerable{System.String})"]/*'/>
		public void Send( System.Collections.Generic.IEnumerable<System.String> collection ) { 
			this.SendMail( this.BuildMessage( collection ) );
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Mail.MessageElement"]/member[@name="Send(System.Collections.Generic.IEnumerable{System.String},System.Object[])"]/*'/>
		public void Send( System.Collections.Generic.IEnumerable<System.String> collection, params System.Object[] args ) { 
			this.SendMail( this.BuildMessage( collection, args ) );
		}
		#endregion methods

	}

}