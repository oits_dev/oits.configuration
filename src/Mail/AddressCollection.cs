﻿using System.Linq;

namespace Oits.Configuration.Mail {

	/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Mail.AddressCollection"]/member[@name=""]/*'/>
	[System.Serializable]
	public class AddressCollection : NamedConfigurationElementCollectionBase<AddressElement> { 

		#region .ctor
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Mail.AddressCollection"]/member[@name=".#ctor"]/*'/>
		public AddressCollection() : base() { 
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Mail.AddressCollection"]/member[@name=".#ctor(System.Collections.IComparer)"]/*'/>
		public AddressCollection( System.Collections.IComparer comparer ) : base( comparer ) { 
		}
		#endregion .ctor


		#region methods
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Mail.AddressCollection"]/member[@name="ToMailAddressCollection()"]/*'/>
		public System.Net.Mail.MailAddressCollection ToMailAddressCollection() { 
			var output = new System.Net.Mail.MailAddressCollection();

			foreach ( var addy in this.OfType<AddressElement>().Where( 
				x => ( null != x ) 
			).Select( 
				x => x.ToMailAddress() 
			).Where( 
				x => ( null != x ) 
			) ) { 
				output.Add( addy );
			}

			return output;
		}
		#endregion methods


		#region static methods
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Mail.AddressCollection"]/member[@name="op_Explicit(Oits.Configuration.Mail.AddressCollection)~System.Net.Mail.MailAddressCollection"]/*'/>
		public static explicit operator System.Net.Mail.MailAddressCollection( AddressCollection collection ) { 
			if ( null == collection ) { 
				return null;
			} else { 
				return collection.ToMailAddressCollection();
			}
		}
		#endregion static methods

	}

}