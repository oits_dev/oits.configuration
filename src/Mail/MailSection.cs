﻿namespace Oits.Configuration.Mail {

	/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Mail.MailSection"]/member[@name=""]/*'/>
	[System.Serializable]
	public class MailSection : System.Configuration.ConfigurationSection { 

		#region fields
		/// <summary>The default name of the <see cref="T:Oits.Configuration.Mail.MailSection"/> element in the configuration file.</summary>
		public const System.String DefaultSectionName = "oits.mail";
		#endregion fields


		#region .ctor
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Mail.MailSection"]/member[@name=".#ctor"]/*'/>
		public MailSection() : base() { 
		}
		#endregion .ctor


		#region properties
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Mail.MailSection"]/member[@name="Messages"]/*'/>
		[System.Configuration.ConfigurationProperty( "", IsDefaultCollection = true, IsRequired = false )]
		[System.Configuration.ConfigurationCollection( typeof( MessageCollection ), 
			AddItemName = "add", 
			ClearItemsName = "clear", 
			RemoveItemName = "remove" 
		)]
		public MessageCollection Messages { 
			get { 
				return (MessageCollection)base[ "" ];
			}
		}
		#endregion properties


		#region static methods
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Mail.MailSection"]/member[@name="GetSection()"]/*'/>
		public static MailSection GetSection() { 
			return ( System.Configuration.ConfigurationManager.GetSection( Oits.Configuration.Mail.MailSection.DefaultSectionName ) as MailSection );
		}
		#endregion static methods

	}

}