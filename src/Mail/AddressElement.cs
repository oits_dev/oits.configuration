﻿namespace Oits.Configuration.Mail { 

	/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Mail.AddressElement"]/member[@name=""]/*'/>
	[System.Serializable]
	public class AddressElement : System.Configuration.ConfigurationElement, INamedConfigurationElement { 

		#region .ctor
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Mail.AddressElement"]/member[@name=".#ctor()"]/*'/>
		public AddressElement() : base() { 
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Mail.AddressElement"]/member[@name=".#ctor(System.String)"]/*'/>
		public AddressElement( System.String name ) : this() { 
			this.DisplayName = name;
		}
		#endregion .ctor


		#region properties
		System.String INamedConfigurationElement.Name { 
			get { 
				return this.DisplayName;
			}
			set { 
				this.DisplayName = value;
			}
		}

		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Mail.AddressElement"]/member[@name="Address"]/*'/>
		[System.Configuration.ConfigurationProperty( "address", DefaultValue = "", IsRequired = true, IsKey = false )]
		public System.String Address { 
			get { 
				return (System.String)this[ "address" ];
			}
			set { 
				this[ "address" ] = value;
			}
		}

		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Mail.AddressElement"]/member[@name="DisplayName"]/*'/>
		[System.Configuration.ConfigurationProperty( "displayName", DefaultValue = "", IsRequired = false, IsKey = false )]
		public System.String DisplayName { 
			get { 
				return (System.String)this[ "displayName" ];
			}
			set { 
				this[ "displayName" ] = value;
			}
		}
		#endregion properties


		#region methods
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Mail.AddressElement"]/member[@name="ToMailAddress()"]/*'/>
		public System.Net.Mail.MailAddress ToMailAddress() { 
			return new System.Net.Mail.MailAddress( this.Address, this.DisplayName );
		}
		#endregion methods


		#region static methods
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.Mail.AddressElement"]/member[@name="op_Explicit(Oits.Configuration.Mail.AddressElement)~System.Net.Mail.MailAddress"]/*'/>
		public static explicit operator System.Net.Mail.MailAddress( AddressElement element ) { 
			if ( null == element ) { 
				return null;
			} else { 
				return element.ToMailAddress();
			}
		}
		#endregion static methods

	}

}