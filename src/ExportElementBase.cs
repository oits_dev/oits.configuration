﻿using System.Linq;

namespace Oits.Configuration { 

	[System.Serializable]
	public abstract class ExportElementBase : System.Configuration.ConfigurationElement, IExport { 

		#region fields
		protected const System.String DefaultEncoding = "US-ASCII";
		protected const System.String DefaultCulture = "en-US";
		#endregion fields


		#region .ctor
		protected ExportElementBase() : base() { 
		}
		protected ExportElementBase( System.String name ) : this() { 
			this.Name = name;
		}
		#endregion .ctor


		#region properties
		[System.Configuration.ConfigurationProperty( "name", DefaultValue = "", IsRequired = true, IsKey = true )]
		public virtual System.String Name { 
			get { 
				return (System.String)this[ "name" ];
			}
			set { 
				this[ "name" ] = value;
			}
		}

		[System.Configuration.ConfigurationProperty( "cultureName", DefaultValue = DefaultCulture, IsRequired = false )]
		public virtual System.String CultureName { 
			get { 
				return (System.String)this[ "cultureName" ] ?? DefaultCulture;
			}
			set { 
				this[ "cultureName" ] = value;
			}
		}
		[System.Configuration.ConfigurationProperty( "useDefaultCulture", DefaultValue = true, IsRequired = false )]
		public virtual System.Boolean UseDefaultCulture {
			get {
				return (System.Boolean)( this[ "useDefaultCulture" ] ?? true );
			}
			set {
				this[ "useDefaultCulture" ] = value;
			}
		}

		[System.Configuration.ConfigurationProperty( "skipHeader", DefaultValue = false, IsRequired = false )]
		public virtual System.Boolean SkipHeader {
			get {
				return (System.Boolean)( this[ "skipHeader" ] ?? false );
			}
			set {
				this[ "skipHeader" ] = value;
			}
		}

		[System.Configuration.ConfigurationProperty( "paddingChar", DefaultValue = null, IsRequired = false )]
		public virtual System.Nullable<System.Char> PaddingChar {
			get {
				return (System.Nullable<System.Char>)this[ "paddingChar" ];
			}
			set {
				this[ "paddingChar" ] = value;
			}
		}
		[System.Configuration.ConfigurationProperty( "recordSeparater", DefaultValue = "\r\n", IsRequired = false )]
		public virtual System.String RecordSeparater {
			get {
				return (System.String)this[ "recordSeparater" ] ?? "\r\n";
			}
			set {
				this[ "recordSeparater" ] = value;
			}
		}
		[System.Configuration.ConfigurationProperty( "fieldSeparater", DefaultValue = '\t', IsRequired = false )]
		public virtual System.Nullable<System.Char> FieldSeparater {
			get {
				return (System.Nullable<System.Char>)this[ "fieldSeparater" ];
			}
			set {
				this[ "fieldSeparater" ] = value;
			}
		}
		[System.Configuration.ConfigurationProperty( "quoteChar", DefaultValue = '\"', IsRequired = false )]
		public virtual System.Char QuoteChar {
			get {
				return (System.Char)( this[ "escapeChar" ] ?? '\"' );
			}
			set {
				this[ "quoteChar" ] = value;
			}
		}
		[System.Configuration.ConfigurationProperty( "escapeChar", DefaultValue = '\"', IsRequired = false )]
		public virtual System.Char EscapeChar {
			get {
				return (System.Char)( this[ "escapeChar" ] ?? '\"' );
			}
			set {
				this[ "escapeChar" ] = value;
			}
		}

		[System.Configuration.ConfigurationProperty( "encoding", DefaultValue = DefaultEncoding, IsRequired = false )]
		public virtual System.String Encoding { 
			get { 
				return (System.String)this[ "encoding" ] ?? DefaultEncoding;
			}
			set { 
				this[ "encoding" ] = value;
			}
		}
		#endregion properties


		#region methods
		public virtual System.IO.Stream WriteExport( System.Collections.IEnumerable collection ) { 
			if ( null == collection ) { 
				throw new System.ArgumentNullException( "collection" );
			}

			var stream = new System.IO.MemoryStream();
			this.WriteExport( stream, this.GetEncoding(), collection );
			stream.Seek( 0L, System.IO.SeekOrigin.Begin );

			return stream;
		}
		public virtual void WriteExport( System.String filePathName, System.Collections.IEnumerable collection ) { 
			if ( null == collection ) { 
				throw new System.ArgumentNullException( "collection" );
			}

			using ( var stream = new System.IO.FileStream( 
				filePathName, 
				System.IO.FileMode.OpenOrCreate, 
				System.IO.FileAccess.Write, 
				System.IO.FileShare.Read 
			) ) { 
				this.WriteExport( stream, this.GetEncoding(), collection );
				stream.SetLength( stream.Position );
			}
		}
		public abstract void WriteExport( System.IO.Stream stream, System.Text.Encoding encoding, System.Collections.IEnumerable collection );

		protected System.String GetHeaderRow( System.Text.Encoding encoding, System.Collections.Generic.IEnumerable<IExportColumn> columns ) { 
			var fsc = this.FieldSeparater;
			var fss = ( fsc.HasValue ) 
				? new System.String( fsc.Value, 1 ) 
				: System.String.Empty 
			;
			var qChar = this.QuoteChar;
			var eChar = this.EscapeChar;
			var paddingChar = this.PaddingChar;
			System.String svalue = null;
			System.String ovalue = null;
			System.Text.StringBuilder output = new System.Text.StringBuilder();
			var cols = columns.ToArray();
			IExportColumn col = null;
			System.Int32 stop = cols.Length - 1;
			if ( !this.SkipHeader ) { 
				for ( var i = 0; i < stop; i ++ ) { 
					col = cols[ i ];
					svalue = col.HeaderText ?? col.PropertyName ?? col.Name;
					if ( !System.String.IsNullOrEmpty( svalue ) ) { 
						ovalue = this.StringToOutputString( col.HeaderText, fsc, eChar, qChar, col.Length, paddingChar );
						output = output.Append( ovalue ?? System.String.Empty );
					}
					output = output.Append( fss );
				}
				col = cols[ stop ];
				svalue = col.HeaderText ?? col.PropertyName ?? col.Name;
				if ( !System.String.IsNullOrEmpty( svalue ) ) { 
					ovalue = this.StringToOutputString( col.HeaderText, fsc, eChar, qChar, col.Length, paddingChar );
					output = output.Append( ovalue ?? System.String.Empty );
 				}
			}
			return output.ToString();
		}
		protected System.String ValueToString( System.Object value, System.String nullReplacementText, System.String formatString, System.Globalization.CultureInfo cultureInfo ) { 
			return ( null == value ) 
				? nullReplacementText ?? System.String.Empty 
				: System.String.Format( 
					cultureInfo, 
					formatString ?? "{0}", 
					value 
				) 
			;
		}
		protected System.String StringToOutputString( System.String value, System.Nullable<System.Char> fieldSeparater, System.Char escapeChar, System.Char quoteChar, System.Nullable<System.Int32> length, System.Nullable<System.Char> paddingChar ) { 
			if ( null == value ) { 
				throw new System.ArgumentNullException( "value" );
			} else if ( length.HasValue ) { 
				if ( length.Value < 0 ) { 
					throw new System.ArgumentOutOfRangeException( "length" );
				} else if ( 0 == length.Value ) { 
					length = null;
				}
			}
			var vlen = value.Length;
			System.Int32 len = ( length ?? vlen );
			if ( len < value.Length ) { 
				value = value.Substring( 0, len );
			} else if ( ( vlen < len ) && paddingChar.HasValue ) { 
				value = value + new System.String( paddingChar.Value, len - vlen );
			}
			var builder = new System.Text.StringBuilder( len );
			System.Boolean wrap = false;
			System.Func<System.Char, System.Boolean> checkFs;
			if ( fieldSeparater.HasValue ) { 
				checkFs = x => ( !wrap && fieldSeparater.Equals( x ) );
			} else { 
				checkFs = x => false;
			}
			foreach ( var c in value ) { 
				if ( c.Equals( quoteChar ) ) { 
					builder = builder.Append( escapeChar );
				} else if ( checkFs( c ) ) { 
					wrap = true;
					checkFs = x => false;
				}
				builder = builder.Append( c );
			}
			var qString = new System.String( quoteChar, 1 );
			var output = ( wrap ) 
				? qString + builder.ToString() + qString 
				: builder.ToString() 
			;
			return output;
		}

		public System.Globalization.CultureInfo GetCulture() { 
			return ( this.UseDefaultCulture ) 
				? System.Globalization.CultureInfo.CurrentUICulture 
				: System.Globalization.CultureInfo.CreateSpecificCulture( this.CultureName ?? DefaultCulture ) 
			;
		}
		public System.Text.Encoding GetEncoding() { 
			return System.Text.Encoding.GetEncoding( this.Encoding ?? DefaultEncoding );
		}
		#endregion methods

	}

}