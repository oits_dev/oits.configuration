﻿using System.Linq;

namespace Oits.Configuration {

	/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.ConfigurationElementCollectionBase`1"]/member[@name=""]/*'/>
	[System.Serializable]
	public abstract class ConfigurationElementCollectionBase<T> : System.Configuration.ConfigurationElementCollection where T : System.Configuration.ConfigurationElement, new() { 

		#region .ctor
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.ConfigurationElementCollectionBase`1"]/member[@name=".#ctor"]/*'/>
		protected ConfigurationElementCollectionBase() : base() { 
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.ConfigurationElementCollectionBase`1"]/member[@name=".#ctor(System.Collections.IComparer)"]/*'/>
		protected ConfigurationElementCollectionBase( System.Collections.IComparer comparer ) : base( comparer ) { 
		}
		#endregion .ctor


		#region properties
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.ConfigurationElementCollectionBase`1"]/member[@name="IndexOf(`0)"]/*'/>
		public virtual System.Int32 IndexOf( T element ) { 
			return this.BaseIndexOf( element );
		}
		#endregion properties


		#region methods
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.ConfigurationElementCollectionBase`1"]/member[@name="CreateNewElement()"]/*'/>
		protected override System.Configuration.ConfigurationElement CreateNewElement() { 
			return new T();
		}

		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.ConfigurationElementCollectionBase`1"]/member[@name="Add(`0)"]/*'/>
		public virtual void Add( T element ) { 
			if ( null == element ) { 
				throw new System.ArgumentNullException( "element" );
			}
			this.BaseAdd( element );
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.ConfigurationElementCollectionBase`1"]/member[@name="RemoveAt(System.Int32)"]/*'/>
		public virtual void RemoveAt( System.Int32 index ) { 
			if ( index < 0 ) { 
				throw new System.ArgumentOutOfRangeException( "index" );
			}
			this.BaseRemoveAt( index );
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.ConfigurationElementCollectionBase`1"]/member[@name="Clear()"]/*'/>
		public virtual void Clear() { 
			this.BaseClear();
		}
		#endregion methods

	}

}