﻿namespace Oits.Configuration.Export { 

	[System.Serializable]
	public class ExportSection : System.Configuration.ConfigurationSection { 

		#region fields
		public const System.String DefaultSectionName = "oits.export";
		#endregion fields


		#region .ctor
		public ExportSection() : base() { 
		}
		#endregion .ctor


		#region properties
		[System.Configuration.ConfigurationProperty( "", IsDefaultCollection = true, IsRequired = false )]
		[System.Configuration.ConfigurationCollection( typeof( ExportGroupCollection ),
			AddItemName = "add", 
			ClearItemsName = "clear", 
			RemoveItemName = "remove" 
		)]
		public ExportGroupCollection Groups { 
			get { 
				return (ExportGroupCollection)base[ "" ];
			}
		}
		#endregion properties


		#region static methods
		public static ExportSection GetSection() { 
			return ( System.Configuration.ConfigurationManager.GetSection( Oits.Configuration.Export.ExportSection.DefaultSectionName ) as ExportSection );
		}
		#endregion static methods

	}

}