﻿using System.Linq;

namespace Oits.Configuration.Export { 

	[System.Serializable]
	public class ExportElement : ExportElementBase { 

		#region .ctor
		public ExportElement() : base() { 
		}
		public ExportElement( System.String name ) : base( name ) { 
		}
		#endregion .ctor


		#region properties
		[System.Configuration.ConfigurationProperty( "", IsDefaultCollection = true, IsRequired = false )]
		[System.Configuration.ConfigurationCollection( typeof( ColumnCollection ), 
			AddItemName = "add", 
			ClearItemsName = "clear", 
			RemoveItemName = "remove" 
		)]
		public ColumnCollection Columns { 
			get { 
				return (ColumnCollection)base[ "" ];
			}
		}
		#endregion properties


		#region methods
		public override void WriteExport( System.IO.Stream stream, System.Text.Encoding encoding, System.Collections.IEnumerable collection ) { 
			if ( null == collection ) { 
				throw new System.ArgumentNullException( "collection" );
			} else if ( null == encoding ) { 
				throw new System.ArgumentNullException( "encoding" );
			} else if ( null == stream ) { 
				throw new System.ArgumentNullException( "stream" );
			} else if ( !stream.CanWrite ) { 
				throw new System.ArgumentException( "The stream must be writable.", "stream" );
			}

			if ( !this.SkipHeader ) { 
				this.WriteHeader( stream, encoding );
			}

			System.Collections.IEnumerator ge = null;
			try { 
				ge = collection.GetEnumerator();
				if ( null == ge ) { 
					return;
				}
				if ( !ge.MoveNext() ) { 
					return;
				}
				this.WriteRecords( stream, encoding, ge );
			} catch ( System.Exception ) { 
				throw;
			} finally { 
				var id = ( ge as System.IDisposable );
				if ( null != id ) { 
					id.Dispose();
				}
			}
		}

		private void WriteHeader( System.IO.Stream stream, System.Text.Encoding encoding ) { 
			if ( null == encoding ) { 
				throw new System.ArgumentNullException( "encoding" );
			} else if ( null == stream ) { 
				throw new System.ArgumentNullException( "stream" );
			} else if ( !stream.CanWrite ) { 
				throw new System.ArgumentException( "The stream must be writable.", "stream" );
			}

			System.String ovalue = null;
			System.Byte[] buffer = null;
			var cols = this.Columns.OfType<IColumnElement>().ToArray();
			if ( !this.SkipHeader ) { 
				ovalue = this.GetHeaderRow( encoding, cols );
				if ( !System.String.IsNullOrEmpty( ovalue ) ) { 
					buffer = encoding.GetBytes( ovalue );
					if ( 0 < buffer.Length ) { 
						stream.Write( buffer, 0, buffer.Length );
					}
				}
			}
		}
		private void WriteRecords( System.IO.Stream stream, System.Text.Encoding encoding, System.Collections.IEnumerator iter ) { 
			if ( null == iter ) { 
				throw new System.ArgumentNullException( "iter" );
			} else if ( null == encoding ) { 
				throw new System.ArgumentNullException( "encoding" );
			} else if ( null == stream ) { 
				throw new System.ArgumentNullException( "stream" );
			} else if ( !stream.CanWrite ) { 
				throw new System.ArgumentException( "The stream must be writable.", "stream" );
			}

			System.Object record = null;
			do { 
				record = iter.Current;
			} while ( ( null == record ) && iter.MoveNext() );
			if ( null == record ) { 
				return;
			}
			System.Byte[] rs = null;
			var rss = this.RecordSeparater ?? System.String.Empty;
			if ( !System.String.IsNullOrEmpty( rss ) ) { 
				rs = encoding.GetBytes( rss );
				if ( 0 < rs.Length ) { 
					stream.Write( rs, 0, rs.Length );
				}
			}

			var fsc = this.FieldSeparater;
			System.Byte[] fs = null;
			if ( fsc.HasValue ) { 
				fs = encoding.GetBytes( new System.Char[] { fsc.Value } );
			}
			var type = record.GetType();
			var cols = this.Columns.OfType<IColumnElement>().ToArray();
			var map = this.Columns.GetProperties( type );
			var myCulture = this.GetCulture();
			System.Reflection.PropertyInfo pi;
			IColumnElement column = null;
			var colCount = cols.Length;
			var colStop = colCount - 1;
			System.Boolean next = false;
			do { 
				record = iter.Current;
				for ( System.Int32 i = 0; i < colStop; i++ ) { 
					column = cols[ i ];
					pi = map[ column ];
					this.WriteColumn( stream, encoding, record, pi, myCulture, column );
					if ( null != fs ) { 
						stream.Write( fs, 0, fs.Length );
					}
				}
				column = cols[ colStop ];
				pi = map[ column ];
				this.WriteColumn( stream, encoding, record, pi, myCulture, column );

				next = iter.MoveNext();
				if ( next ) { 
					if ( 0 < rs.Length ) { 
						stream.Write( rs, 0, rs.Length );
					}
				}
			} while ( next );
		}
		private void WriteColumn( System.IO.Stream stream, System.Text.Encoding encoding, System.Object record, System.Reflection.PropertyInfo propertyInfo, System.Globalization.CultureInfo cultureInfo, IColumnElement column ) { 
			var value = column.GetValue( record, propertyInfo, cultureInfo );
			var svalue = this.ValueToString( value, column.NullReplacementText, column.FormatString, cultureInfo );
			var ovalue = this.StringToOutputString( svalue, this.FieldSeparater, this.EscapeChar, this.QuoteChar, column.Length, this.PaddingChar );
			if ( !System.String.IsNullOrEmpty( ovalue ) ) { 
				var buffer = encoding.GetBytes( ovalue );
				if ( 0 < buffer.Length ) { 
					stream.Write( buffer, 0, buffer.Length );
				}
			}
		}
		#endregion methods

	}

}