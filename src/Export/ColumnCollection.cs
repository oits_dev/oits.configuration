﻿using System.Linq;

namespace Oits.Configuration.Export { 

	[System.Serializable]
	public class ColumnCollection : NamedConfigurationElementCollectionBase<ColumnElement> { 

		#region .ctor
		public ColumnCollection() : base() { 
		}
		public ColumnCollection( System.Collections.IComparer comparer ) : base( comparer ) { 
		}
		#endregion .ctor


		#region methods
		public System.Collections.Generic.IDictionary<IColumnElement, System.Reflection.PropertyInfo> GetProperties( System.Type type ) { 
			if ( null == type ) { 
				throw new System.ArgumentNullException( "type" );
			}

			var map = new System.Collections.Generic.Dictionary<IColumnElement, System.Reflection.PropertyInfo>( System.Math.Max( 1, this.Count ) );
			System.Reflection.PropertyInfo pi;
			foreach ( var c in this.OfType<IColumnElement>().Where( 
				x => ( null != x ) 
			) ) { 
				if ( c.TryGetProperty( type, out pi ) ) { 
					map.Add( c, pi );
				}
			}
			return map;
		}
		#endregion methods

	}

}