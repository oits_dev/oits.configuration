﻿using System.Linq;

namespace Oits.Configuration.Export { 

	[System.Serializable]
	public class ExportCollection : NamedConfigurationElementCollectionBase<ExportElement> { 

		#region .ctor
		public ExportCollection() : base() { 
		}
		public ExportCollection( System.Collections.IComparer comparer ) : base( comparer ) { 
		}
		#endregion .ctor

	}

}