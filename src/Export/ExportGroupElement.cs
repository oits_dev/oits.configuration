﻿using System.Linq;
using Oits.Configuration.Export;

namespace Oits.Configuration.Export {

	[System.Serializable]
	public class ExportGroupElement : System.Configuration.ConfigurationElement, INamedConfigurationElement { 


		#region .ctor
		public ExportGroupElement() : base() { 
		}
		public ExportGroupElement( System.String name ) : this() { 
			this.Name = name;
		}
		#endregion .ctor


		#region properties
		[System.Configuration.ConfigurationProperty( "name", IsRequired = true, IsKey = true )]
		public System.String Name { 
			get { 
				return (System.String)base[ "name" ];
			}
			set { 
				base[ "name" ] = value;
			}
		}

		[System.Configuration.ConfigurationProperty( "", IsDefaultCollection = true, IsRequired = false )]
		[System.Configuration.ConfigurationCollection( typeof( ExportCollection ), 
			AddItemName = "add", 
			ClearItemsName = "clear", 
			RemoveItemName = "remove" 
		)]
		public ExportCollection Exports { 
			get { 
				return (ExportCollection)base[ "" ];
			}
		}
		#endregion properties

	}

}