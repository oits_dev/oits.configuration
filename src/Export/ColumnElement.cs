﻿using System.Linq;

namespace Oits.Configuration.Export { 

	[System.Serializable]
	public class ColumnElement : ExportColumnBase, IColumnElement { 

		#region .ctor
		public ColumnElement() : base() { 
		}
		public ColumnElement( System.String name ) : base( name ) { 
		}
		#endregion .ctor


		#region properties
		[System.Configuration.ConfigurationProperty( "propertyName", IsRequired = true )]
		public override System.String PropertyName { 
			get { 
				return base.PropertyName;
			}
			set { 
				base.PropertyName = value;
			}
		}

		[System.Configuration.ConfigurationProperty( "index", DefaultValue = (System.String)null, IsRequired = false )]
		public System.String Index { 
			get { 
				return (System.String)this[ "index" ];
			}
			set { 
				this[ "index" ] = value;
			}
		}
		#endregion properties


		#region methods
		public System.Object GetValue( System.Object record ) { 
			if ( null == record ) { 
				return null;
			}
			return this.GetValue( record, this.GetProperty( record.GetType() ) );
		}
		public System.Object GetValue( System.Object record, System.Reflection.PropertyInfo propertyInfo ) { 
			if ( null == record ) { 
				return null;
			} else if ( null == propertyInfo ) { 
				return record;
			}
			return this.GetValue( record, propertyInfo, null );
		}
		public System.Object GetValue( System.Object record, System.Reflection.PropertyInfo propertyInfo, System.Globalization.CultureInfo culture ) { 
			if ( null == record ) { 
				return null;
			} else if ( null == propertyInfo ) { 
				return record;
			}
			System.Object[] indexes = ( System.String.IsNullOrEmpty( this.Index ) ) 
				? null 
				: this.Index.Split( new System.Char[] { ',' }, System.StringSplitOptions.RemoveEmptyEntries ).Select( 
						x => x.Trim() 
					).ToArray() 
			;
			return propertyInfo.GetValue( 
				record, 
				System.Reflection.BindingFlags.IgnoreCase | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.GetProperty, 
				null, 
				indexes, 
				culture 
			 );
		}

		public System.Reflection.PropertyInfo GetProperty( System.Type type ) { 
			var propertyName = this.PropertyName;
			if ( System.String.IsNullOrEmpty( propertyName ) ) { 
				return null;
			}
			return type.GetProperty( propertyName, System.Reflection.BindingFlags.IgnoreCase | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.GetProperty );
		}
		public System.Boolean TryGetProperty( System.Type type, out System.Reflection.PropertyInfo propertyInfo ) { 
			propertyInfo = null;

			System.Boolean output = false;
			try { 
				propertyInfo = this.GetProperty( type );
				output = true;
			} catch ( System.Exception ) { 
				;
			}
			return output;
		}
		#endregion methods

	}

}