﻿using System.Linq;

namespace Oits.Configuration.Export { 

	public interface IColumnElement : IExportColumn { 

		System.Reflection.PropertyInfo GetProperty( System.Type type );
		System.Boolean TryGetProperty( System.Type type, out System.Reflection.PropertyInfo propertyInfo );

		System.Object GetValue( System.Object store );
		System.Object GetValue( System.Object store, System.Reflection.PropertyInfo propertyInfo, System.Globalization.CultureInfo culture );

	}

}