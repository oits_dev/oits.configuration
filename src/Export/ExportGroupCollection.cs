﻿using System.Linq;

namespace Oits.Configuration.Export { 

	[System.Serializable]
	public class ExportGroupCollection : NamedConfigurationElementCollectionBase<ExportGroupElement> { 

		#region .ctor
		public ExportGroupCollection() : base() { 
		}
		public ExportGroupCollection( System.Collections.IComparer comparer ) : base( comparer ) { 
		}
		#endregion .ctor

	}

}