﻿using System.Linq;

namespace Oits.Configuration { 

	[System.Serializable]
	public abstract class ExportColumnBase : System.Configuration.ConfigurationElement, IExportColumn { 

		#region .ctor
		protected ExportColumnBase() : base() { 
		}
		protected ExportColumnBase( System.String name ) : this() { 
			this.Name = name;
		}
		#endregion .ctor


		#region properties
		[System.Configuration.ConfigurationProperty( "name", IsRequired = true, IsKey = true )]
		public virtual System.String Name {
			get {
				return (System.String)this[ "name" ];
			}
			set {
				this[ "name" ] = value;
			}
		}

		public virtual System.String PropertyName { 
			get { 
				return (System.String)this[ "propertyName" ];
			}
			set { 
				this[ "propertyName" ] = value;
			}
		}

		[System.Configuration.ConfigurationProperty( "headerText", DefaultValue = null, IsRequired = false )]
		public virtual System.String HeaderText { 
			get { 
				return (System.String)this[ "headerText" ];
			}
			set { 
				this[ "headerText" ] = value;
			}
		}

		[System.Configuration.ConfigurationProperty( "formatString", DefaultValue = "{0}", IsRequired = false )]
		public System.String FormatString {
			get {
				return (System.String)this[ "formatString" ] ?? "{0}";
			}
			set {
				this[ "formatString" ] = value;
			}
		}

		[System.Configuration.ConfigurationProperty( "skip", DefaultValue = false, IsRequired = false )]
		public System.Boolean Skip {
			get {
				return (System.Boolean)( this[ "skip" ] ?? false );
			}
			set {
				this[ "skip" ] = value;
			}
		}

		[System.Configuration.ConfigurationProperty( "length", DefaultValue = null, IsRequired = false )]
		public virtual System.Nullable<System.Int32> Length { 
			get { 
				return (System.Nullable<System.Int32>)this[ "length" ];
			}
			set { 
				this[ "length" ] = value;
			}
		}

		[System.Configuration.ConfigurationProperty( "nullReplacementText", DefaultValue = "", IsRequired = false )]
		public virtual System.String NullReplacementText {
			get {
				return (System.String)this[ "nullReplacementText" ] ?? System.String.Empty;
			}
			set {
				this[ "nullReplacementText" ] = value;
			}
		}
		#endregion properties

	}

}