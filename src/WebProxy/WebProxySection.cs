﻿using System.Linq;

namespace Oits.Configuration.WebProxy { 

	/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.WebProxy.WebProxySection"]/member[@name=""]/*'/>
	[System.Serializable]
	public class WebProxySection : System.Configuration.ConfigurationSection { 

		#region fields
		/// <summary>The default name of the <see cref="Oits.Configuration.WebProxy.WebProxySection" /> element in the configuration file.</summary>
		public const System.String DefaultSectionName = "oits.webProxy";
		#endregion fields


		#region .ctor
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.WebProxy.WebProxySection"]/member[@name=".#ctor"]/*'/>
		public WebProxySection() : base() { 
		}
		#endregion .ctor


		#region properties
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.WebProxy.WebProxySection"]/member[@name="ServerUrls"]/*'/>
		[System.Configuration.ConfigurationProperty( "", IsDefaultCollection = true, IsRequired = false )]
		[System.Configuration.ConfigurationCollection( typeof( ServerUrlCollection ), 
			AddItemName = "add", 
			ClearItemsName = "clear", 
			RemoveItemName = "remove" 
		)]
		public ServerUrlCollection ServerUrls { 
			get { 
				return (ServerUrlCollection)base[ "" ];
			}
		}

		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.WebProxy.WebProxySection"]/member[@name="MustMatch"]/*'/>
		[System.Configuration.ConfigurationProperty( "mustMatch", DefaultValue = false, IsRequired = false, IsKey = false )]
		public System.Boolean MustMatch { 
			get { 
				return (System.Boolean)( this[ "mustMatch" ] ?? false );
			}
			set { 
				this[ "mustMatch" ] = value;
			}
		}
		#endregion properties


		#region methods
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.WebProxy.WebProxySection"]/member[@name="GetServerUrlElement(System.String)"]/*'/>
		public ServerUrlElement GetServerUrlElement( System.String uri ) { 
			var element = this.ServerUrls[ uri ];
			if ( null != element ) { 
				return element;
			}

			element = this.ServerUrls.OfType<ServerUrlElement>().Where( 
				x => ( 
						System.String.Equals( uri, x.ServerUrl, System.StringComparison.InvariantCultureIgnoreCase ) 
						|| ( x.MatchStart && uri.StartsWith( x.ServerUrl, System.StringComparison.InvariantCultureIgnoreCase ) ) 
					) 
			).FirstOrDefault();
			if ( null != element ) { 
				return element;
			}

			if ( this.MustMatch ) { 
				throw new System.InvalidOperationException();
			}

			return null;
		}
		#endregion methods


		#region static methods
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.WebProxy.WebProxySection"]/member[@name="GetSection()"]/*'/>
		public static WebProxySection GetSection() { 
			return ( System.Configuration.ConfigurationManager.GetSection( Oits.Configuration.WebProxy.WebProxySection.DefaultSectionName ) as WebProxySection );
		}
		#endregion static methods

	}

}