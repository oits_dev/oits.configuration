﻿namespace Oits.Configuration.WebProxy { 

	/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.WebProxy.ServerUrlCollection"]/member[@name=""]/*'/>
	[System.Serializable]
	public class ServerUrlCollection : NamedConfigurationElementCollectionBase<ServerUrlElement> { 

		#region .ctor
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.WebProxy.ServerUrlCollection"]/member[@name=".#ctor"]/*'/>
		public ServerUrlCollection() : base() { 
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.WebProxy.ServerUrlCollection"]/member[@name=".#ctor(System.Collections.IComparer)"]/*'/>
		public ServerUrlCollection( System.Collections.IComparer comparer ) : base( comparer ) { 
		}
		#endregion .ctor

	}

}