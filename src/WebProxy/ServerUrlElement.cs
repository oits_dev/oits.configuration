﻿namespace Oits.Configuration.WebProxy { 

	/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.WebProxy.ServerUrlElement"]/member[@name=""]/*'/>
	[System.Serializable]
	public class ServerUrlElement : System.Configuration.ConfigurationElement, INamedConfigurationElement { 

		#region .ctor
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.WebProxy.ServerUrlElement"]/member[@name=".#ctor"]/*'/>
		public ServerUrlElement() : base() { 
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.WebProxy.ServerUrlElement"]/member[@name=".#ctor(System.String)"]/*'/>
		public ServerUrlElement( System.String name ) : this() { 
			this.ServerUrl = name;
		}
		#endregion .ctor


		#region properties
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.WebProxy.ServerUrlElement"]/member[@name="ServerUrl"]/*'/>
		[System.Configuration.ConfigurationProperty( "serverUrl", DefaultValue = (System.String)null, IsRequired = true, IsKey = true )]
		public System.String ServerUrl { 
			get { 
				return ( this[ "serverUrl" ] as System.String );
			}
			set { 
				this[ "serverUrl" ] = value;
			}
		}
		System.String INamedConfigurationElement.Name { 
			get { 
				return this.ServerUrl;
			}
			set { 
				this.ServerUrl = value;
			}
		}

		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.WebProxy.ServerUrlElement"]/member[@name="MatchStart"]/*'/>
		[System.Configuration.ConfigurationProperty( "matchStart", DefaultValue = false, IsRequired = false, IsKey = false )]
		public System.Boolean MatchStart { 
			get { 
				return (System.Boolean)( this[ "matchStart" ] ?? false );
			}
			set { 
				this[ "matchStart" ] = value;
			}
		}

		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.WebProxy.ServerUrlElement"]/member[@name="TokenParameter"]/*'/>
		[System.Configuration.ConfigurationProperty( "tokenParameter", DefaultValue = "token", IsRequired = false, IsKey = false )]
		public System.String TokenParameter { 
			get { 
				return ( this[ "tokenParameter" ] as System.String );
			}
			set { 
				this[ "tokenParameter" ] = value;
			}
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.WebProxy.ServerUrlElement"]/member[@name="TokenParameterName"]/*'/>
		[System.Configuration.ConfigurationProperty( "tokenParameterName", DefaultValue = "token", IsRequired = false, IsKey = false )]
		public System.String TokenParameterName { 
			get { 
				return ( this[ "tokenParameterName" ] as System.String );
			}
			set { 
				this[ "tokenParameterName" ] = value;
			}
		}

		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.WebProxy.ServerUrlElement"]/member[@name="TokenHeader"]/*'/>
		[System.Configuration.ConfigurationProperty( "tokenHeader", DefaultValue = (System.String)null, IsRequired = false, IsKey = false )]
		public System.String TokenHeader { 
			get { 
				return ( this[ "tokenHeader" ] as System.String );
			}
			set { 
				this[ "tokenHeader" ] = value;
			}
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.WebProxy.ServerUrlElement"]/member[@name="TokenHeaderName"]/*'/>
		[System.Configuration.ConfigurationProperty( "tokenHeaderName", DefaultValue = (System.String)null, IsRequired = false, IsKey = false )]
		public System.String TokenHeaderName { 
			get { 
				return ( this[ "tokenHeaderName" ] as System.String );
			}
			set { 
				this[ "tokenHeaderName" ] = value;
			}
		}

		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.WebProxy.ServerUrlElement"]/member[@name="CredentialName"]/*'/>
		[System.Configuration.ConfigurationProperty( "credentialName", DefaultValue = (System.String)null, IsRequired = false, IsKey = false )]
		public System.String CredentialName { 
			get { 
				return ( this[ "credentialName" ] as System.String );
			}
			set { 
				this[ "credentialName" ] = value;
			}
		}
		#endregion properties


		#region methods
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.WebProxy.ServerUrlElement"]/member[@name="ToParameter"]/*'/>
		public System.String ToParameter() { 
			var name = this.TokenParameterName;
			var value = this.TokenParameter;
			if ( !System.String.IsNullOrEmpty( value ) && !System.String.IsNullOrEmpty( name ) ) { 
				return name + "=" + value;
			} else { 
				return null;
			}
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.WebProxy.ServerUrlElement"]/member[@name="BuildParameter(System.String)"]/*'/>
		public System.String BuildParameter( System.String uri ) { 
			if ( System.String.IsNullOrEmpty( uri ) ) { 
				throw new System.ArgumentNullException( "uri" );
			}

			var parameter = this.ToParameter();
			if ( !System.String.IsNullOrEmpty( parameter ) ) { 
				System.Text.StringBuilder output = new System.Text.StringBuilder( uri );
				output = output.Append( uri.Contains( "?" ) ? "&" : "?" );
				output = output.Append( parameter );
				return output.ToString();
			} else { 
				return uri;
			}
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Configuration.WebProxy.ServerUrlElement"]/member[@name="SetHeader(System.Net.WebRequest)"]/*'/>
		public void SetHeader( System.Net.WebRequest request ) { 
			if ( null == request ) { 
				throw new System.ArgumentNullException( "request" );
			}
			var headerName = this.TokenHeaderName;
			if ( System.String.IsNullOrEmpty( headerName ) ) { 
				return;
			}

			if ( null == request.Headers ) { 
				request.Headers = new System.Net.WebHeaderCollection();
			}
			request.Headers.Add( headerName, this.TokenHeader );
		}
		#endregion methods

	}

}